# ionode_simulator

## Few words about Data

### Data Format

Data should be a csv file with some mandatary fields such as:

```
- scenario: Unique name/id of each application. Format does not matter, but every application should be unique
- forwarders: Number of forwarders. Each couple (scenario, fowarders) should be unique.
- bandwith: Bandwidth of the application with n forwarders
```

Additionnaly the data could contains fields `knowledge_bdw` and `profile`' that represents the normed bandwith of the application profile and the profile's name. Each profile has a unique value for couple (forwarders, knowledge_bdw). It represents the knowledge of each profile and it represented bandwidth.

### Get applications knowledge

A dedicated python script could find and annotates data with `profile` and `knowledge_bdw` info.

```
python3 regression_on_data.py
```

Currently it is requiered to change input and ouput file inside code.
It creates `profile` and `knowledge_bdw` fields as well as `norm_bdw` that represents the normalised bandwidth of each applicaiton for each forwarder number. This normalization is based on max, where max value of application is 1. Formula is `norm_bdw(n) = b(n) / b(nperf)`.


## Step 1: Workload generation

This is the first step of experiment workflow. It creates batch of applications following user parameters.
In short, a dedicated python script creates the folder hierarchy and all applications batch in JSON format.

To run applications' batch generation:
```
PYTHONPATH=. python3 ionsimulator/xp_generator.py -c <configuration file>
```

```
usage: IO Node xP generator [-h] -c CONFIG

Generate and save applications set

options:
  -h, --help            show this help message and exit
  -c CONFIG, --config   configuration file
```

### Configuration files

Configuration file is a file that contains all experiments parameters such as number of node, number of applicaiton, occupanyc etc...

Possible parameters are the following:

```
- (*) output_folder:  root folder where experiments results will be stored
- (*) input_data:     csv files, as described earlier
- (*) number of node. It could be a list or a range description, mutually exclusive and at least one option:
  - node_list: list of all combinaison to test
  - max_node, min_node and node_step: three parameters that describe the range of number of node tested
- (*) number of application. It could be a list or a range description, mutually exclusive and at least one option:
  - app_list: list of all combinaison to test
  - max_app, min_app and app_step: three parameters that describe the range of application of node tested
- (*) occ_node, occ_node and occ_step: three parameters that describe the range of number of node tested
- (*) nb_iteration: length of application.
- (*) nb_xp: number of experiment with each combinaison of parameters
- (*) random_seed: seed for random number generator (python random)
- is_knowledge: Says whenever application return knowledge bandwidth or real bandwidth.
```

Example file that generate 100 experiments for (#apps, #nodes) = [(10,20), (20, 20), (40,20)] at occupancy °.1 to 0.9 (step of 0.1). Applications will give only knowledge:

```
{
    "output_folder":"data/io_nodes",
    "input_data":"data/marenostrum_forge.csv",
    
    "node_list": [20],

    "app_list": [10, 20, 40],

    "min_occ":0.1,
    "max_occ":1,
    "occ_step":0.1,

    "nb_iteration": 5000,
    "nb_xp":100,
    "random_seed":1,

    "is_knowledge":1

}
```

### Folder hierarchy

This section describes folder hierarchy of output folder.
Inside input folder are created multiple folders nameds `<occ>_<nb_app>_<nb_node>`.
Inside each folder are stored a generation folder, for batch generation, and multiples JSON for simulation results

For previous configuration example the hierarchy would be the following:
```
data/io_nodes
├── 0.10_10_20
│   └── generation
├── 0.10_20_20
│   └── generation
.
.
.
├── 0.90_20_20
│   └── generation
├── 0.90_40_20
│   └── generation

```

Experiments are named `<xp_id>_<allocator>_<placement>.json`.
Generation are simply named `<xp_id>.json`.

### JSON format

#### Generation files

Files with application batch.
Named `generation/<xp_id>.json`

```
- apps_gen: list of applications
  - id: application's id
  - nsys: application's nsys
  - nperf: applicaiton's nperf
  - bdw_arr: array of bandwidth, where index match with fwd_arr
  - fwd_arr: array with all n possible for application
  - n_cpu: number of compute node taken by application
  - nb_iter: runtime 
  - tcpu: aggregated compute time
  - tio: aggregated I/O time
  - v_io: Amount of I/O to transfert during execution
  - nb_phase: number of Compute/IO phase

- IO_load: real I/O load of the application batch
- target_load: targeted load (generation parameter)
- n_node: number of I/O resources
```

#### Experiment files

Files obtained after simulation ran.
Named `<xp_id>_<allocator>_<placement>.json`.

```
- stop_iter: end of observed time window
- scheduler_load: I/O obtained after allocation
- allocator: allocator used for simulation
- placement: placement used for simulation

- app_simulation: list of applications' state at end of simulation
  - id: application ID that match with generation file
  - stop_it: iteration at wich application stopped
  - ideal_io_t: I/O time application would have run in isolation
  - io_t: measured I/O time during simulation
  - cpu_t: measured compute time during simulation
  - tcp_o_tio: time compute / time I/O
  - nb_col: number of iteration with colission during execution
  - bdw_arr: same as generation file. Used for manuel checking that everything is alright
  - node_data: information on all node application run onto
    - node_id: Unique ID of the node
    - node_col: number of colision on that particular node for that application
    
- node_simulation: list of nodes' state at end of simulation
  - stop_iteration: iteration where node stopped
  - id: unique node ID
  - nb_app: number of application scheduled on node
  - nb_col: number of colision on node
  - t_io: IO time run by node
  - t_cpu: idle time of node
  - res_occ: ?
  - sum_rio: sum of application ratio of I/O over Compute time
```

## Step 2: Run experiments

There is multiple script to launch experiments:

- Single setting runner: Could launch experiments with one set of settings.
- Mpi launcher: launch all experiments from configuration file with multiple MPI process
- Slurm script: Use slurm to schedule MPI script

### Single parameter experiments


```
PYTHONPATH=. python3 ionsimulator/runner.py -h
usage: IO Node Runner [-h] -n NB_NODE [-o OUTPUT] --occ OCC [-a NB_APP] [-x NB_XP] [-s STOP_ITERATION]
                      [--start_id START_ID] [--mckp] [--bestbdw] [--bestbdwsmart] [--randomallocator]
                      [--nsys] [--tcpu] [--bestbdwnode] [--static]

Run experiments with one number of I/O node

options:
  -h, --help            show this help message and exit
  -n NB_NODE, --nb_node NB_NODE
                        Number of I/O node
  -o OUTPUT, --output OUTPUT
                        Output directory (csv files)
  -a NB_APP, --nb_app NB_APP
                        Number of application
  -x NB_XP, --nb_xp NB_XP
                        Number of experiment to run on each scheduler
  -s STOP_ITERATION, --stop_iteration STOP_ITERATION
                        Stop iteration for metric measure
  --start_id START_ID   Start iteration id. Mean for splitting experiments.
  --mckp                Adding MCKP experiments
  --bestbdw             Adding allocator BestBdw to experiments.
  --bestbdwsmart        Adding allocator SmartBestNiAllocator_0.4 to experiments.
  --randomallocator     Adding allocator RandomAllocator to experiments.
  --nsys                Adding allocator NiSystem to experiments.
  --tcpu                Adding allocator TCPUAllocator to experiments.
  --bestbdwnode         Adding allocator BestBdwPerNode to experiments.
  --static              Adding allocator StaticAllocator to experiments.
```

Example:
`PYTHONPATH=. python3 ionsimulator/runner.py -n 20 -a 20 -x 3 -s 100 --bestbdw --nsys -o data/testallocators `

Would launch experiments with 20 nodes, 20 applications with stop iteration at 100. Repeat 3 times. Takes generation files from `data/testalocators` folder and save results in the same folder. It evaluates best bandwith allocaiton policy and nsys allocator policy with all distributors (random, greedy clairvoyant and non-clairvoyant).


### Automatic MPI script

3 MPI launcher exists:

```
- mpi_launcher_ion.c :       All experiments with marenostrum data, without knowledge (configuration/complete_gen.json)
- mpi_launcher_ion_k.c       All experiments with marenostrum data, with knowledge (configuration/complete_gen_k.json) 
- mpi_launcher_ion_ost.c     All experiments with ost data, without knowledge (configuration/complete_gen_ost.json)
```

They are compiled with `make` and run with `mpiexec`:

Example:
`mpiexec -np 5 ./mpi_launcher_ion_k`

### Slurm scripts

Severals slurm script are provided. But they are tuned for PlaFrim (machine name and configuration).

To run them simply launch `sbatch run.sbatch`. `run.sbatch` could be replace by `run_knowledge.sbatch` or `run_ost.sbatch`.

## Step 3: Plot experiments

`TODO: Clean script that is mostly manual tuning for each figure.`
