#include <mpi.h>
#include <unistd.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

const int TOTAL_NB_XP = 100;
const int XP_CHUNCKS = 4;
const int XP_CHUNCK_SIZE = TOTAL_NB_XP / XP_CHUNCKS;

int count_lines(FILE *fd) {
  if (fd == NULL) {
    printf("Null pointer in count_lines.\nExit.\n");
    exit(EXIT_FAILURE);
  }

  int c;
  int nb_lines = 0;

  while (!feof(fd)) {
    c = fgetc(fd);
    if (c == '\n')
      nb_lines++;
  }

  return nb_lines;
}

void get_starting_line_offset(FILE *fd, int line_number) {
        if (fd == NULL) {
                printf("Null pointer in get_starting_line_offset.\nExit.\n");
                exit(EXIT_FAILURE);
        }

        // go back to beggining
        if (fseek(fd, 0, SEEK_SET) == -1){
                printf("lseek error in get_starting_line_offset.\nExit.\n");
                exit(EXIT_FAILURE);
        }

        char line[256];
        int line_size = 256;
        int readed_lines = 0;
        while(readed_lines < line_number) {
                fgets(line, line_size, fd);
                readed_lines++;
        }
        printf("Stopped line %d\n", readed_lines);
}

void read_csv_line(FILE *fd, char **occ, char **n_app, char **n_node,
                   char **xp_id) {

        if (fd == NULL) {
                printf("Null pointer in read_csv_line.\nExit.\n");
                exit(EXIT_FAILURE);
        }

        char line[1024];
        unsigned long position = ftell(fd);
        fgets(line, 1024, fd);
        char* tmp = strdup(line);
        *occ = strtok(tmp, ",");
        *n_node = strdup(strtok(NULL, ","));
        *n_app = strdup(strtok(NULL, ","));
        *xp_id = strdup(strtok(NULL, ","));

        printf("OCC is %s\n", *occ);
        free(tmp);
}


int main(int argc, char *argv[]) {
  // Initialize MPI and rank
  MPI_Init(NULL, NULL);
  int world_size, world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // measure time
  clock_t time;
  time = clock();

  // init all experiement parameters
  if (world_rank == 0) {
    printf("Lanching experiment on %d process\n", world_size);
    printf("Creating all experiments settings\n");
    char gen_cmd[255];
    sprintf(gen_cmd, "PYTHONPATH=. python3 ionsimulator/xp_generator.py -c "
                     "configuration/complete_gen_ost.json");
    system(gen_cmd);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  // launch python scripts
  FILE* xp_files = fopen("data/ost/xp_gen_data.csv", "r");
  int nb_xp = count_lines(xp_files) - 1; // remove header
  int xp_per_rank = nb_xp / world_size;
  int start_idx = xp_per_rank * world_rank;
  int end_idx = start_idx + xp_per_rank;
  char py_parameters[1024];
  char command[2048];

  if (end_idx < nb_xp) {
    for (int i = start_idx; i < end_idx; i++) {
            char* occ;
            char* n_app;
            char* n_node;
            char* xp_id;
            printf("Rank %d tries to read line %i\n", world_rank, i);
            get_starting_line_offset(xp_files, i+1);
        char line[1024];
        fgets(line, 1024, xp_files);
        char* tmp = strdup(line);
        occ = strtok(tmp, ",");
        n_node = strdup(strtok(NULL, ","));
        n_app = strdup(strtok(NULL, ","));
        xp_id = strdup(strtok(NULL, ","));            
            
      sprintf(py_parameters,
              "-n %s --occ %s -a %s -x 1 -s 3000 --bestbdw --randomallocator --nsys --tcpu --static --mckp -o data/ost  --start_id %s",
              n_node, occ, n_app,
              xp_id);
      sprintf(command, "PYTHONPATH=. python3 ionsimulator/runner.py %s",
              py_parameters);

      printf("Process %d launching xp with parameters %s %s %s %s (app, ion, "
             "occ, start id)\n",
             world_rank, n_app, n_node,
             occ, xp_id);

      printf("command: %s\n", command);
      system(command);
      free(tmp);
    }
  }

  fclose(xp_files);
  MPI_Barrier(MPI_COMM_WORLD);

  if (world_rank == 0) {
    time = clock() - time;
    double time_taken = ((double)time) / CLOCKS_PER_SEC;
    printf("Script is now over.\nIt tooks %lf second to run.", time_taken);
  }

  MPI_Finalize();
  return 0;
}
