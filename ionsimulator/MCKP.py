from io_node import IONode
from applications.real_application import RealApplication

# import for saving state in case of bug
import pickle
from time import time
from json import dump


STATE_SAVING_PATH = "data/"


class MCKPScheduler:
    """
    Applies the MCKP scheduling algorithm from (Bez et al. 2021)
    The apply method will decide the number of I/O nodes to use
    per application, create and return the list of I/O nodes with
    the applications already placed on them.
    """

    def __init__(self):
        pass

    def pickle_state(self, filename: str, obj) -> None:
        """ save the state of scheduler with application and nodes
        this is usefull to reproduce scheduling bug """
        with open(filename, 'w') as fd:
            pickle.dump(obj, fd)

    def dump_state(self, solution: dict, nodes: list):
        bug_time = int(time())
        filename = f"{bug_time}"
        self.pickle_state(STATE_SAVING_PATH + filename + ".mckpnodes", nodes)
        self.pickle_state(STATE_SAVING_PATH + filename + ".mckpsol", solution)
        with open(STATE_SAVING_PATH + filename + "_solution" + ".json", 'w') as fd:
            dump(solution, fd)

    def apply(self, nb_ion: int, apps: list) -> list:
        """
        Apply the policy and returns the list of IO nodes
        with applications placed on them.
        """
        # Step 1) Parse the database
        # for each application, we store a list of pairs
        # of number of I/O nodes and achieved bandwidth, i.e.
        # app_data[application] is a list of (#I/O nodes used, bandwidth)
        # While doing that, we remove useless pairs (where we use
        # more I/O nodes to reach lower performance).
        # TODO do we want to add more I/O node options ? Do we do it here ?
        app_data = []
        for i, app in enumerate(apps):
            app_data.append([])  # list per application
            perf = app.get_bdw_array_k()
            ion_list = app.get_ni_array()
            assert 1 in ion_list
            index_of_1 = -1
            found_best = False
            best_perf = max(perf)
            for ion_index, ion in enumerate(ion_list):
                # Ignores measurements with zero I/O nodes
                if ion == 0:
                    continue
                if ion == 1:
                    index_of_1 = ion_index
                if found_best:
                    continue #we don't need to add options that cost more
                            # I/O nodes to reach lower performance, they
                            # will never be chosen
                            # TODO this could be better (but it should not
                            #impact simulation results...)
                if perf[ion_index] == best_perf:
                    found_best = True
                # Since we need to use integers and we are using five precision
                # points, convert it
                bandwidth = int(perf[ion_index] * 100000.0)
                # adds the pair (#I/O nodes used, bandwidth)
                app_data[i].append((ion, bandwidth))
            #we add an option with cost 0 giving a rough and pessimistic
            #estimation for its performance. The idea is that we avoid
            #sharing as much as possible, but the option exists because
            #otherwise we will not find a solution when there are more
            #apps than io nodes
            assert index_of_1 >= 0
            app_data[i].append((0, perf[index_of_1]/len(apps)))
        assert len(app_data) == len(apps)

        #we apply mckp but with number of I/O nodes -1 (because
        #we added an option of cost 0 to each application, which
        #correspond to sharing a single io node, so we have to
        #reserve that i/o node that would be used for sharing)
        solution = self._apply_mckp(nb_ion -1, app_data)
        assert len(solution) == len(apps)
        #but we check the solution. If it does not use that shared
        #I/O node, then we can use the actual number of i/o nodes
        if len([solution[app] for app in solution if solution[app] == 0]) == 0:
            for i in range(len(app_data)):
                assert solution[i] > 0
                app_data[i] = [pair for pair in app_data[i] if pair[0] > 0]
            solution = self._apply_mckp(nb_ion, app_data)
            assert len(solution) == len(apps)
            assert len([solution[app] for app in solution if solution[app] == 0]) == 0
            solution_with_sharing = False
        else:
            solution_with_sharing = True
        #now that we have the solution, set the ni of each
        #application and place them on the io nodes
        nodes = []
        shared = None
        for app_index in solution:
            if solution[app_index] > 0:
                apps[app_index].reconfigure_ni(solution[app_index])
                for ion in range(solution[app_index]):
                    nodes.append(IONode())
                    nodes[-1].add_app(apps[app_index])
            elif shared == None:
                apps[app_index].reconfigure_ni(1)
                assert solution[app_index] == 0
                nodes.append(IONode())
                nodes[-1].add_app(apps[app_index])
                shared = nodes[-1]
            else:
                apps[app_index].reconfigure_ni(1)
                assert solution[app_index] == 0
                shared.add_app(apps[app_index])
        assert len(nodes) <= nb_ion
        shared_nodes = len([node for node in nodes if len(node.get_apps()) > 1])
        if solution_with_sharing:
            if shared_nodes > 1:
                print("WARNING! MCKP made a solution with a single shared node, but there are", shared_nodes, "with more than one application on them")
                print("MCKP solution was:")
                print(solution)
                print("=============================")
                self.dump_state(solution, nodes)
        else:
            if len(nodes) > 0:
                print("WARNING! MCKP made a solution without sharing nodes, but there are", shared_nodes, "with more than one application on them")
                print("MCKP solution was:")
                print(solution)
                print("=============================")
                self.dump_state(solution, nodes)
        assert (solution_with_sharing and shared_nodes == 1) or ((not solution_with_sharing) and shared_nodes == 0)
        return nodes

    def _apply_mckp(self, nb_ion: list, app_data: list) -> list:

        # Step 2) Initialize the solution and the current_bw status array
        # solution: list of dictionaries containing one dictionary per
        # number of I/O nodes in the system. Each dictionary contains
        # keys related to applications and values related to the index
        # to the best pair (#I/O nodes used, bandwidth) for a given intermediary
        # solution
        # current_bw: used to store the total bandwidth achieved so far
        # for a subset of applications sharing the I/O nodes in the system
        solution = [dict() for i in range(nb_ion + 1)]
        current_bw = [-1 for i in range(nb_ion + 1)]
        # fills the starting solution with information from the
        # first application (0) in the list
        application = 0
        for pair_index, pair in enumerate(app_data[application]):
            ion, bandwidth = pair
            # if the system has enough I/O nodes
            if ion <= nb_ion:
                # if the bandwidth is better than what stored previously
                # (it should always be true in this step)
                if bandwidth > current_bw[ion]:
                    # stores the BW for this number of I/O nodes
                    current_bw[ion] = bandwidth
                    # stores the index to this pair in the solution
                    solution[ion][application] = pair_index

        # Step 3) Construct the solution with the remaining applications
        for application in range(1, len(app_data)):
            # moves current bw to a list of previous bw solutions and
            # starts a new, fresh current_bw array
            previous_bw = current_bw
            current_bw = [-1 for i in range(nb_ion + 1)]
            # for each number of I/O nodes possible for this application
            for pair_index, pair in enumerate(app_data[application]):
                ion, bandwidth = pair
                # look for viable solutions that could be expanded to
                # include this one
                for system_io_nodes in range(ion, nb_ion + 1):
                    # if it is possible to expand this previous solution to
                    # include this number of I/O nodes for this application
                    if previous_bw[system_io_nodes - ion] > 0:
                        accumulated_bw = previous_bw[system_io_nodes - ion] + bandwidth
                        # checks if its better than what we found so far
                        # for this total number of I/O nodes used
                        if accumulated_bw >= current_bw[system_io_nodes]:
                            current_bw[system_io_nodes] = accumulated_bw
                            # stores the index of this pair in the solution
                            solution[system_io_nodes][application] = pair_index

        # Step 4) Find the number of nodes used for the best solution
        # Index (#I/O nodes) for the highest achieved bandwidth
        # (in the case of a tie, takes the one that uses the least I/O nodes)
        best_io_nodes = current_bw.index(max(current_bw))
        assert best_io_nodes > 0
        # prepares return values
        selected_nodes = dict()
        # goes from the last to the first application
        remaining_nodes = best_io_nodes
        for app_i in range(len(app_data)):
            # recovers the index of the pair (ion, bandwidth) for this app
            i = len(app_data) - app_i - 1
            pair_index = solution[remaining_nodes][i]
            ion, bandwidth = app_data[i][pair_index]
            selected_nodes[i] = ion
            # removes the I/O nodes used by this application from the
            #remaining ones
            remaining_nodes = remaining_nodes - ion
        # last sanity check before returning the results
        assert sum(selected_nodes.values()) <= nb_ion
        assert sum(selected_nodes.values()) == best_io_nodes

        return selected_nodes



    def __str__(self):
        return "MCKPAllocator"


    def __repr__(self):
        return self.__str__()
