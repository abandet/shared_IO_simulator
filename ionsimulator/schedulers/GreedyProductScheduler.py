import operator


class GreedyProductScheduler:
    """ Schedule job on IONodes """

    def __init__(self):
        self.seed = 42

    def schedule_apps(self, machine):
        """ Order app on machine"""
        apps = sorted(machine.get_pending_apps(),
                      key=operator.attrgetter('tio'), reverse=True)
        for app in apps:
            for _ in range(app.get_ni()):
                node = self.get_emptiest_node(machine, app)
                node.add_app(app)

        # empty pending apps
        # machine.empty_apps()
        # generates ionodes collisions vectors
        machine.generate_collision()

    def get_emptiest_node(self, machine, app):
        nodes = sorted(machine.ionodes, key=operator.methodcaller(
            'get_score', method='factor'))
        for n in nodes:
            if not n.is_app_in(app):
                return n
        raise Exception('get_emptiest_node: no available node.')

    def __str__(self):
        return "Greedy product"
