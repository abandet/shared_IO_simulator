from random import sample, choices
from schedulers.SchedulerInterface import SchedulerInterface


class RandomScheduler(SchedulerInterface):
    """ Schedule job on IONodes """

    def __init__(self):
        self.seed = 42

    def schedule_apps(self, machine):
        """ Order app on machine"""
        for app in machine.get_pending_apps():
            # select io nodes for app (random process)
            nodes = sample(
                range(0, machine.get_nb_ionodes()), app.get_ni())
            for n in nodes:
                ionode = machine.get_nth_ionode(n)
                ionode.add_app(app)

    def __str__(self):
        return "Random"

    def schedule_application(self, machine, application):
        """ Schedule a single application on machine"""
        nodes = choices(machine.ionodes, k=application.ni)
        for ion in nodes:
            ion.add_app(application)
        return [ion.id for ion in nodes]
