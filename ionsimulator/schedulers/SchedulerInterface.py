import operator
import random


class SchedulerInterface:
    def __init__(self, subgroups=1):
        self.seed = 42
        self.name = "Scheduler Interface"
        self.subgroups = subgroups

    def schedule_apps(self, machine):
        """ Schedule app on machine """
        self.schedule_apps_subgroup(machine, self.subgroups)

    def schedule_apps_subgroup(self, machine, subgroups=1):
        """ schedule apps on machine's ionodes by sub group
        in two steps scheduling.
            First step creates subgroups and split applications in
        each subgroup, with load balance consideration.
            Second step does a classic scheduling of each subroups
        of application on each subgroups of ionode """
        # check if number of subgroups is multiple of nb apps
        if self.subgroups is None:
            self.subgroups = 1
            subgroups = 1
        if len(machine.get_ionodes()) % subgroups != 0:
            raise ValueError(
                "scheduling : nb subgroups should be a multiple of nb ionodes")

        # create sub group of applications
        apps = sorted(machine.get_pending_apps(),
                      key=operator.attrgetter('tio'), reverse=True)
        apps_groups = [[] for _ in range(subgroups)]
        # balance apps groups (fairness)
        for app in apps:
            lightiest_app = self._get_emptiest_apps_group(apps_groups)
            apps_groups[lightiest_app].append(app)

        # create sub group of IO nodes
        ion = machine.get_ionodes()
        subgroup_len = int(len(ion) / subgroups)
        io_nodes_groups = [ion[i:i+subgroup_len]
                           for i in range(0, len(ion)-subgroup_len+1, subgroup_len)]  # noqa

        # schedule application on I/O nodes
        self._schedule_groups(io_nodes_groups, apps_groups)

    def _schedule_groups(self, ionodes_groups: list, apps_groups: list) -> None:  # noqa
        """ Schedules each apps group on each ionodes group """
        for ions, apps in zip(ionodes_groups, apps_groups):
            self._schedule_apps(ions, apps)

    def _schedule_apps(self, io_nodes: list, applications: list) -> None:
        """ schedule applications on ionodes """
        # sort app descending by Tio
        apps = sorted(applications, key=operator.attrgetter(
            'tio'), reverse=True)
        # schedule apps on nodes
        nodes = []
        for app in apps:
            for _ in range(app.get_ni()):
                random.shuffle(io_nodes)  # avoid id priority
                node = self._emptiest_node(io_nodes, app)
                node.add_app(app)
                nodes.append(node.id)
        return nodes

    def _emptiest_node(self, io_nodes: list, app):
        raise NotImplementedError("Cannot get score in Interface")

    def _get_emptiest_apps_group(self, groups):
        min_score = self._get_apps_score(groups[0])
        min_idx = 0
        for i, g in enumerate(groups):
            if self._get_apps_score(g) < min_score:
                min_score = self._get_apps_score(g)
                min_idx = i
        return min_idx

    def _get_apps_score(self, app_group: list) -> float:
        raise NotImplementedError("Cannot get score in Interface")

    def __str__(self):
        return self.name

    def schedule_application(self, machine, application):
        """ Schedule a single application on machine"""
        nodes = self._schedule_apps(machine.ionodes, [application])
        return nodes
