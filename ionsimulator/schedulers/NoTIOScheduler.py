import operator
from schedulers.SchedulerInterface import SchedulerInterface


class NoTIOScheduler(SchedulerInterface):
    """ Schedule IO on node with less application """

    def __init__(self):
        pass

    def schedule_apps(self, machine):
        """ Order app on machine"""
        apps = sorted(machine.get_pending_apps(),
                      key=operator.attrgetter('tio'), reverse=True)

        for app in apps:
            for _ in range(app.get_ni()):
                node = self.get_emptiest_node(machine, app)
                node.add_app(app)

    def get_emptiest_node(self, machine, app):
        nodes = sorted(machine.ionodes, key=operator.methodcaller(
            'get_score', method='NoTIO'))
        # avoid app twice on node
        for n in nodes:
            if not n.is_app_in(app):
                return n
        raise Exception('get_emptiest_node: no available node.')

    def _emptiest_node(self, ionodes: list, app):
        nodes = sorted(ionodes, key=operator.methodcaller(
            'get_score', method='NoTIO'))
        # avoid app twice on node
        for n in nodes:
            if not n.is_app_in(app):
                return n
        raise Exception('_emptiest_node: no available node.')

    def __str__(self):
        return "No TIO scheduler"
