import operator
from schedulers.SchedulerInterface import SchedulerInterface


class GreedySumScheduler(SchedulerInterface):
    """ Schedule job on IONodes """

    def __init__(self, subgroups=None):
        super().__init__()
        self.name = "Greedy Sum"
        self.subgroups = subgroups
        if subgroups is not None:
            self.name += f" {subgroups} subg"
        else :
            self.subgroups = 1

    def _get_apps_score(self, app_group: list) -> float:
        if len(app_group) == 0:
            return 0

        score = 0.0
        for app in app_group:
            score += app.get_tio()

        return score

    def _emptiest_node(self, io_nodes: list, app):
        """ returns the emptiest node without app """
        nodes = sorted(io_nodes, key=operator.methodcaller(
            'get_score', method='sum'))
        for n in nodes:
            if not n.is_app_in(app):
                return n

        raise Exception('_emptiest_node_position: no available node.')
