import operator
from schedulers.SchedulerInterface import SchedulerInterface


class GreedyProduct2Scheduler(SchedulerInterface):
    """ Schedule job on IONodes """

    def __init__(self, subgroups=None):
        super().__init__()
        self.name = "Greedy 2by2 factor"
        self.subgroups = subgroups
        if subgroups is not None:
            self.name += f" {subgroups} subg"

    def _get_apps_score(self, app_group: list) -> float:
        if len(app_group) == 0:
            return 0

        score = 0.0
        for app in app_group:
            for app2 in app_group:
                if app is not app2:
                    score += app.get_tio() * app2.get_tio()

        return score

    def _emptiest_node(self, io_nodes: list, app):
        """ returns the emptiest node without app """
        nodes = sorted(io_nodes, key=operator.methodcaller(
            'get_score', method='factor2'))
        for n in nodes:
            if not n.is_app_in(app):
                return n

        raise Exception('_emptiest_node_position: no available node.')
