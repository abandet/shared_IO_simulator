#!/usr/bin/env python3

from TestSession import TestSession
from schedulers.GreedyProduct2Scheduler import GreedyProduct2Scheduler
from schedulers.RandomScheduler import RandomScheduler
from schedulers.GreedySumScheduler import GreedySumScheduler
from schedulers.NoTIOScheduler import NoTIOScheduler

blocking = 'local'

# # test local schedulers
schedulers = [RandomScheduler(), GreedySumScheduler(), NoTIOScheduler()]

test = TestSession(10, schedulers, min_app=8,
                   max_app=46, app_step=2, nb_nodes=20)
test.run_and_save()
