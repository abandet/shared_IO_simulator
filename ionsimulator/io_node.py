"""
IO resource module
"""
from random import choice
from copy import deepcopy
import itertools


class IONode:
    """ Simulate IO Node behavior """

    class_id = itertools.count()

    def __init__(self):
        self.apps = []
        self.collision_vector = []
        self.iteration_vector = []
        self.waiting_apps = []
        self.id = next(IONode.class_id)

    def get_res_occ(self, stop_iteration) -> float:
        """Compute ressource occupancy of the node. This is the proportion of
        time the node spent doing I/O on the windos [0:stop_iteration]

        :param stop_iteration: 
        :returns:

        """
        t_io = self.iteration_vector[:min(
            len(self.iteration_vector), stop_iteration)].count(1)

        return t_io / stop_iteration

    def get_apps(self) -> list:
        return self.apps

    def add_app(self, app):
        """ Add one application to IONode """
        self.apps.append(app)

    def __str__(self):
        return f"IONode(#apps={len(self.apps)}, sum_rio={self.get_sum_rio()})"

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        return len(self.apps) < len(other.apps)

    def __eq__(self, other):
        if isinstance(other, IONode):
            return self.id == other.id
        return False

    def is_app_in(self, app):
        return app in self.apps

    def get_sum_rio(self) -> float:
        return sum(a.get_rio() for a in self.apps)

    def run_iter(self):
        # check io apps
        io_app = [app for app in self.apps if app.is_doing_io(self.id)]
        # return if no apps running
        if not io_app:
            self.iteration_vector.append(0)
            self.collision_vector.append(0)
            return

        # if app founded then it runs I/O !
        self.iteration_vector.append(1)
        if len(io_app) > 1:
            self.collision_vector.append(-1)
        else:
            self.collision_vector.append(0)
        # choose running app
        # update waiting app list
        self.update_waiting_list(io_app)
        app = self.choose_running_app()
        # pop app from list and update iteration_vector
        for idx, apps in enumerate(self.waiting_apps):
            if apps[0] == app:
                self.waiting_apps.pop(idx)

        # send colision message to all blocked app
        for a in self.waiting_apps:
            a[0].have_colision(self.id)

        # run app
        app.run_io(self.id)

    def choose_running_app(self):
        # choose app
        app = self.get_oldest_waiting_app()
        # return app
        return app

    def update_waiting_list(self, apps: list) -> None:
        """ Updates waiting list by adding tuple (application, wait time)
        to the list if not already waiting. Otherwise increments wait time
        by one """
        for app in apps:
            # adding app in waiting list
            if app not in [app for app, _ in self.waiting_apps]:
                self.waiting_apps.append([app, 0])
            # increase wainting app wainting time
            else:
                for idx, tup in enumerate(self.waiting_apps):
                    if tup[0] is app:
                        self.waiting_apps[idx][1] += 1
                        break

    def get_oldest_waiting_app(self):
        maxi = 0
        apps = []
        for app, wait_time in self.waiting_apps:
            if wait_time > maxi:
                maxi = wait_time
                apps = [app]
            elif wait_time == maxi:
                apps.append(app)

        return choice(apps)

    # def get_utilization(self, stop_iter: int) -> float:
    #     """
    #     Compute utilization ratio (io/runtime) of the ionode until stop
    #     point define by stop_iter.
    #     Because of geometrique that do no accept 0 value, minimum value returned is 0.00001.

    #     :param stop_iter: Iteration where to stop. If shorter than node runtime then last iteration is taken
    #     """
    #     stop = min(stop_iter, len(self.iteration_vector))
    #     runtime = stop
    #     iotime = self.iteration_vector[:stop].count(1)
    #     assert iotime <= runtime
    #     return max(iotime/runtime, 0.000001)

    # def get_collision(self, stop_iter: int) -> float:
    #     """
    #     Compute collision ratio (#coll/runtime) of the ionode until stop
    #     point define by stop_iter.
    #     Because of geometrique that do no accept 0 value, minimum value returned is 0.00001.

    #     :param stop_iter: Iteration where to stop. If shorter than node runtime then last iteration is taken
    #     """
    #     stop = min(stop_iter, len(self.iteration_vector))
    #     runtime = stop
    #     col_time = self.collision_vector[:stop].count(1)
    #     assert col_time <= runtime
    #     return max(col_time/runtime, 0.000001)

    def save_node_run(self, stop_iteration):
        # assert data coherency
        try:
            assert (len(self.iteration_vector) == len(self.collision_vector))
        except Exception as e:
            print(len(self.iteration_vector))
            print(len(self.collision_vector))
            raise e

        stop_iteration = min(stop_iteration, len(self.iteration_vector))
        d = {}
        d["stop_iteration"] = stop_iteration
        d["id"] = self.id
        d["nb_app"] = len(self.apps)
        d["nb_col"] = self.collision_vector[:stop_iteration].count(-1)
        d["t_io"] = self.iteration_vector[:stop_iteration].count(1)
        d["t_cpu"] = self.iteration_vector[:stop_iteration].count(0)
        d["res_occ"] = self.get_res_occ(stop_iteration)
        d["sum_rio"] = self.get_sum_rio()

        assert d["t_io"] + d["t_cpu"] == stop_iteration

        return d
