import pandas as pd
import matplotlib.pyplot as plt

SAVE_FIG="../img/alpha_gc.png"
DATA = "../data/alpha_gc.csv"


df = pd.read_csv(DATA)

# Plot whisker box
df[df['forwarders'] != 1].boxplot(column='v')
plt.title("Distribution of gain / cost Values")
plt.savefig("../img/distribution_gc.png")
plt.clf()

# Plot lines
df.set_index('forwarders', inplace=True)
df.groupby('scenario')['v'].plot(legend=False, ylabel="Gain/cout compares to n=1")
plt.tight_layout()
plt.grid(visible=True, which='both')
plt.hlines(y=1, xmin=1, xmax=8, color='red', linewidth=2, linestyle="--")
plt.savefig(SAVE_FIG)
plt.clf()
