import copy
from multipledispatch import dispatch
import itertools
import pdb
from copy import deepcopy
import random


class ApplicationInterface:
    """
    Application class interface
    Simple representation of an application

    * Principle
    An application has an ni, representing the number of I/O nodes the application will
    run into. It also has a ratio of time spent doing I/O during its execution, tio, compare
    to time on computation.
    Application will run I/O independently on each of its I/O nodes but an implicit barrier exist
    at the end of an I/O phase.
    I/O phase is a period with consecutive I/O.

    * Technical
    Each application has several array to describe application state:
        - iters: Correspond to the constant array of time step. It should never be modified.
      This represents the behavior of this application in isolation, with 0 for cpu time and
      1 in case of I/O time.
        - run_iters: This array is identical to iters array at the beginning of the execution.
      However it update itself with -1 in case of collision on at least one I/O nodes
        - noder_iter: This is a dictionary with several run_iters like array, one for each
      forwarders allocated to the application, identified with I/O node id (see @IONodes.py).
      These array move independently, exception for implicit barrier at end of I/O phase. In case
      a node is waiting for others to finish, nothing happened in that vector.

    Each array has a pointer corresponding to the current state of the application during its
    execution.

    Run_iter array is empty at beginning of execution. Application adds 0 in case of CPU time,
    -1 if at least one I/O node cannot runs an needed I/O and 1 in case of complet run of I/O,
    over all forwarders.
    """

    newid = itertools.count()

    def __init__(self, nb_iter: int, tio: float, ni: int, ncpu: int = None):
        """ Init the Application

        Parameters:
        nb_iter (int): number of iteration the application will runs.
        tio (float): ratio of time the application spent doing I/O
        ni (int): number of io nodes machine should use for this application

        """
        self.nb_iter = nb_iter
        self.init_nb_iter = nb_iter
        self.ni = ni
        self.init_n = ni
        self.tio = tio
        self.init_tio = tio
        self.nb_stops = 0
        # constant iteration array
        self.iters = self._init_iter_array()
        self.iter_pointer = 0
        # modified array (run history)
        self.run_iters = []
        # keeps track of each node performing I/O
        self.node_iter = self._init_node_iter_array()
        self.last_running_node = None
        # self application identification
        self.id = next(ApplicationInterface.newid)
        # Number of compute node
        self.ncpu = ncpu
        if ncpu is None:
            self.ncpu = random.randint(10, 1000)
        else:
            self.ncpu = ncpu

    def _init_iter_array(self):
        """ Init iteration vector according to application current configuration.
        Raise NotImplementedError in case of call in interface
        """
        raise NotImplementedError("ApplicationInterface")

    def _init_node_iter_array(self) -> list:
        """
        create a list of ni io node dict keeping position of iteration for each ionode
        Each ion has a single dict. That allows the simulator to keep tracks
        of which io iteration the application did on every node.
        """
        return []

    def get_occ(self, stop_iter: int, machine_nb_ion: int) -> float:
        """
        Return the occupancy expected from application (before run),
        with its current ni configuration on machine with machine_nb_ion.

        :param stop_iter:
        :param machine_nb_ion: Total number of I/O node on the machine.
        """
        nb_io = self.iters[:stop_iter].count(1)
        ni = self.ni
        app_occ = (nb_io * ni) / (stop_iter * machine_nb_ion)
        assert app_occ > 0
        return app_occ

    def add_node_iter_array(self, ion_id: int):
        """ add io node entry to application io node list"""
        self.node_iter.append({
            "node_id": ion_id,
            "node_iters": []
        })

    def get_iters(self) -> list:
        """ Returns iteration vector """
        return self.iters

    def get_io_nb_iter(self) -> int:
        """
        Returns the number of iteration of I/O.
        """
        return self.iters.count(1)

    def get_id(self) -> int:
        return self.id

    def get_ni(self):
        """ Returns number of io nodes needed by application """
        return self.ni

    def __str__(self):
        return f"ApplicationInterface<{self.id}>(ni={self.ni}, tio={self.tio}, nb_io={sum(sel.iters)})"

    def __repr__(self):
        return self.__str__()

    def _get_ion_iter_dict(self, ion_id: int) -> dict:
        """ get io nodes dictionnary (id, iter) """
        ion_dict = next(
            (ion for ion in self.node_iter if ion['node_id'] == ion_id), None)
        if ion_dict is None:
            raise ValueError("IO Node no supposed to do IO for this app.")

        return ion_dict

    def is_doing_io(self, ion_id: int) -> bool:
        """ Returns true if the IO node ion_id need to do
        io for its iteration. Used for local blocking"""
        if not self.is_active():
            return False

        if self.iters[self.iter_pointer] == 0:
            return False

        ion_dict = self._get_ion_iter_dict(ion_id)
        # case node has less I/O that current self.iters[:pointer]
        # mean the node needs to do I/O
        if ion_dict["node_iters"].count(1) < self.iters[:self.iter_pointer+1].count(1):
            return True

        # case node has more I/O than current iter. In that case we
        # have to check whenever the node has fninish the I/O phase or
        # not. If finished then it does not run I/O. Else it does.
        return not self.phase_over(ion_id)

    def phase_over(self, ion_id) -> bool:
        """ Return whenever the node with id ion_id has finished it's I/O phase or not

        :param ion_id:
        :returns:

        """
        ion_dict = self._get_ion_iter_dict(ion_id)
        # count total i/O iteration untill next compute phase
        tmp_pointer = self.iter_pointer
        while(tmp_pointer < len(self.iters) and self.iters[tmp_pointer] != 0):
            tmp_pointer += 1
        nb_io_untill_next_cpu = self.iters[:tmp_pointer].count(1)
        nb_io_in_node = ion_dict["node_iters"].count(1)

        # if I/O node has finished it I/O phase then it should have
        # same number of I/O than iters[:tmp_pointer]
        return nb_io_in_node == nb_io_untill_next_cpu

    def run_io(self, ion_id: int) -> None:
        """
        Runs IO on the io node with ion_id
        Updates the node iter vector.

        This function is called by IONode that choose that application to run,
        in case self.is_doing_io(ion_id) is true.
        """
        assert self.is_doing_io(ion_id)
        for ion in self.node_iter:
            if ion["node_id"] == ion_id:
                ion["node_iters"].append(1)
                return
        raise ValueError

    def have_colision(self, node_id):
        """ Update node id with -1, meaning it has been blocked because of a collision.

        :param node_id:
        :returns:

        """
        for n in self.node_iter:
            if n["node_id"] == node_id:
                n["node_iters"].append(-1)
                return


        raise ValueError("Node not founded")

    def run_iter(self) -> None:
        """
        Runs one iteration of application in context of
        local blocking. Update IO Node local status for that
        application and all pointer according to ressources available."""
        if not self.is_active():
            return

        # if compute iter then increments
        if self.iters[self.iter_pointer] == 0:
            self.run_iters.append(0)
            self.iter_pointer += 1
            # avance all node pointer
            for node in self.node_iter:
                node["node_iters"].append(0)
                assert(len(node["node_iters"]) == len(self.run_iters))
            return

        # otherwise : I/O case
        # Does everyone hase finished its phase ?
        phase_over = 0
        for ion in self.node_iter:
            if self.phase_over(ion["node_id"]):
                phase_over += 1
        # yes every one has finished
        if phase_over == len(self.node_iter):
            # all ion with less iteration get -2
            for ion in self.node_iter:
                if len(ion["node_iters"]) == len(self.run_iters):
                    ion["node_iters"].append(-2)
            # go to next compute phase
            self.run_iters.append(1)
            self.iter_pointer += 1
            return
        # if not
        # all ion with less iteration get -2
        for ion in self.node_iter:
            if len(ion["node_iters"]) == len(self.run_iters):
                ion["node_iters"].append(-2)
        # do we need to increment iter_pointer ?
        min_nb_io = self.iters.count(1) + 1
        for ion in self.node_iter:
            nb_io_in_ion = ion["node_iters"].count(1)
            min_nb_io = min(min_nb_io, nb_io_in_ion)
        # if min_nb_io is greater than io in run_iter then it means
        # that every node is ahead run iter so we have to increment it
        # with a 1 and increment pointer
        if min_nb_io > self.run_iters.count(1):
            self.run_iters.append(1)
            self.iter_pointer += 1
        # otherwise a node still need to complete phase
        else:
            self.run_iters.append(-1)


    def is_active(self):
        """
        Returns if application is still running or
        has finish its job
        """
        return self.iter_pointer < len(self.iters)

    def __eq__(self, other):
        return self.id == other.id

    def get_occ(self, machine_nb_ion: int) -> float:
        """returns occupancy helds by application on system with
        machine_nb_ion number of IO nodes """
        return sum(self.iters) * self.ni / (machine_nb_ion * len(self.iters))

    def get_io_time(self, n: int = None) -> int:
        """Returns the number of iteration spent doing I/O in congestion free
        scenario. If n is specified it returns what I/O time would be with n ressources

        :param n: Number of I/O resources
        :returns:

        """
        if n is not None:
            app = deepcopy(self)
            app.reconfigure_ni(n)
            return app.iters.count(1)
        else:
            return self.iters.count(1)

    def get_run_io_time(self, w_start, w_end):
        """Return number of iteration spent doing IO or colision at runtime
        in time window delimited by (w_start, w_end)

        :param w_start: Start iteration for io time observation
        :param w_end: End iteration
        :returns: number of iteration that are not compute

        """
        run_arr = self.run_iters[w_start:w_end]
        cpu_time = run_arr.count(0)
        return len(run_arr) - cpu_time

    def get_compute_time(self) -> int:
        """Returns time spent by application in compute phase in congestion
        free scenario

        :returns:

        """
        return self.iters.count(0)

    def single_io_load(self, ni) -> float:
        raise NotImplementedError("No single io load for interface")

    def get_idle_time(self, start_iter, end_iter):
        """Return Idle time of the application between iteration start_iter and end_iter.
        Idle time is the time spent by compute ressource to wait during IO phase.

        :param start_iter:
        :param end_iter:
        :returns:

        """
        return self.get_run_io_time(start_iter, end_iter) * self.ncpu

    def get_rio(self) -> float:
        """Return Tio / Tcpu at current confifuration

        :returns: 

        """
        raise NotImplementedError()
