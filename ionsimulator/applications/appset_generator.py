from random import randint, choice, shuffle
import numpy as np
from ionsimulator.applications.real_application import RealApplication
from math import log
from copy import deepcopy
import pdb
import os
from statistics import mean

OUTPUT_BOUND_DATA = "data/upper_bound_value.csv"
APP_DATA = "data/app_data.csv"
TIO_DATA = "data/tio_data.csv"
NCPU = 480 # NCPU as factor of nb ion resources
MIN_NUM_PHASES = 2
MAX_NUM_PHASES = 20

class DummyApp:
    """ Dummy app used for storing scenario data (bdw / forwarder)
    and choosing ni. Mainly used for determining application to respect occupancy"""

    def __init__(self, bdw_arr: list, forwarders: list, name: str, knowledge_bdw: list = None):
        self.init_length = 5000
        self.bdw_arr = bdw_arr
        self.forwarders = forwarders
        self.knowledge_bdw = knowledge_bdw
        self.nsys = None # self._get_nsys()
        self.rio = -1.0
        self.name = name
        self.tcpu_o_tio = None

    def _get_nsys(self) -> int:
        """
        Find a temporary nsys, which DOES NOT correspond to the nsys
        defined in the paper. We cannot compute that nsys because for
        now we don't know tcpu and tio.
        However, because we need a nsys for generating the
        applications, we use this temporary nsys, which is calculated
        as the n that minimizes n/b(n)
        """
        n_values = [f / b for f, b in zip(self.forwarders, self.bdw_arr)]
        nsys_value = min(n_values)
        nsys = self.forwarders[n_values.index(nsys_value)]

        #return nsys
        raise NotImplementedError("Should not use this.")

    def get_nsys(self) -> int:
        if self.rio < 0:
            raise ValueError("Negative Rio, cannot compute nsys.")

        if self.rio > 1:
            raise ValueError("Rio greater than 1.")

        tio_1 = self.init_length * self.rio
        tcpu_1 = self.init_length - tio_1
        init_bdw = self.bdw_arr[0]
        
        io_stress_arr = []
        for n in self.forwarders:
            bdw = self.bdw_arr[self.forwarders.index(n)]
            tio = tio_1 * init_bdw / bdw # tio(n) = tio(1) * (b(1)/b(n))
            stress = n*tio / (tio + tcpu_1)
            io_stress_arr.append(stress)

        nsys_stress = min(io_stress_arr)
        nsys_idx = io_stress_arr.index(nsys_stress)
        return self.forwarders[nsys_idx]
        

    def get_nperf(self) -> int:
        return self.forwarders[self.bdw_arr.index(max(self.bdw_arr))]

    def set_rio(self, tcpu_o_tio: float):
        self.rio = 1/(1+tcpu_o_tio)
        self.tcpu_o_tio = tcpu_o_tio

    def get_bdw_arr(self):
        return self.bdw_arr

    def get_forwarders_arr(self):
        return self.forwarders

    def get_tcpu_o_tio(self):
        return self.tcpu_o_tio


class RealAppsSetGenerator:
    """ Generate a set of RealApplication objects, each representing an
    application, with a specified occupancy goal """

    def __init__(self,
                 theta: float,
                 nb_ion: int,
                 nb_apps: int,
                 scenario_data: list,
                 nb_iter: int = 5000,
                 is_knowledge: int = False):
        self.is_knowledge = is_knowledge
        self.target_theta = theta  # * nb_ion
        self.nb_ion = nb_ion
        self.nb_apps = nb_apps
        self.nb_iter = nb_iter
        self.all_app = self._scenario_to_apps(scenario_data)
        self.app_workload = None
        self.Ncpu = NCPU
        # 10% of application getting 75% of cpu nodes (Big App)
        self.nb_big_app = int(nb_apps * 0.1)
        self.nb_cpu_big_app = int(self.Ncpu * 0.75 / self.nb_big_app)
        # 30% of applocation getting 20% of cpu_nodes (Medium app)
        self.nb_med_app = int(nb_apps * 0.3)
        self.nb_cpu_med_app = int(self.Ncpu * 0.20 / self.nb_med_app)
        # small app getting what is left
        self.nb_small_app = int(nb_apps - self.nb_big_app - self.nb_med_app)
        self.nb_cpu_small_app = int((self.Ncpu - self.nb_cpu_big_app * self.nb_big_app - self.nb_cpu_med_app * self.nb_med_app) / self.nb_small_app)

        self.ncpu_arr = [self.nb_cpu_big_app] * self.nb_big_app + [self.nb_cpu_med_app] * self.nb_med_app + [self.nb_cpu_small_app] * self.nb_small_app
        for ncpu in self.ncpu_arr:
            assert(ncpu >= 1)
        # for the remaining ncpu that are not distributed, we give it to the last small app
        offset_ncpu = self.Ncpu - sum(self.ncpu_arr)
        self.ncpu_arr[len(self.ncpu_arr)-1] += offset_ncpu

        assert(sum(self.ncpu_arr) == self.Ncpu)


    def _create_apps(self, apps: list, tcp_o_tio_arr: list) -> list:
        """ Create all application with array of dummy app

        :param apps: Set of Dummy application to convert into RealApplication
        :param r_ios: Array of values representing ratio of I/O time for each application
        :return: List of RealApplication, converted from dummy applications
        """
        workload = []
        for app, tcpu_o_tio, cpu in zip(apps, tcp_o_tio_arr, self.ncpu_arr):
            app.set_rio(tcpu_o_tio)
            a = RealApplication(self.nb_iter, 1, app.get_forwarders_arr(), app.get_bdw_arr(), randint(MIN_NUM_PHASES, MAX_NUM_PHASES), app.get_tcpu_o_tio(), ncpu=cpu, knowledge_bdw=app.knowledge_bdw)
            workload.append(a)

        rios = [a.get_rio() for a in workload]
        out_path = "data/post_gen_data.csv"
        if not os.path.exists(out_path):
            with open(TIO_DATA, 'w') as fd:
                csv_header = "n_app,n_node,target_ioload,r_io\n"
                fd.write(csv_header)
                
        with open(out_path, 'a') as fd:
            csv_data = f"{self.nb_apps},{self.nb_ion},{self.target_theta},{mean(rios)}\n"
            fd.write(csv_data)


        return workload

    def _scenario_to_apps(self, scenarios):
        """Transform the input data, which comes from a csv that
        contains the fields scenario, forwarders (the number of I/O
        resources), and bandwidth to a list of DummyApp objects

        scenario data (all csv data) into dummy application"""
        apps = []
        unique_app = scenarios["scenario"].unique()
        for scenario in unique_app:
            data = scenarios[scenarios["scenario"] == scenario]
            k_bdw = None
            if "knowledge_bdw" in data:
                k_bdw = data["knowledge_bdw"]
            app = DummyApp(list(data["bandwidth"]), list(data["forwarders"]),
                           scenario, knowledge_bdw=k_bdw)
            apps.append(app)

        return apps

    def gen_applications(self) -> list:
        """ Generates an application set with occupancy according
        to self.target_occ.
        It has a while loop that check target occ tolerance is respected
        and if all applications have a correct tio.

        :returns: List of Dummy applications with total occ at target occ

        """
        apps = self._choose_applications()

        tiocf_arr = self._generate_tcpu_o_tio(apps)   #this is not actually io time, but the ration tcpu over tio_cf(1) (greater than 0)

        if tiocf_arr is None:
            return None
        
        workload = self._create_apps(apps, tiocf_arr)
        self.app_workload = workload
        return workload

    def _choose_applications(self):
        #we need deepcopy here because we could select the same application profile multiple times
        return [deepcopy(choice(self.all_app)) for _ in range(self.nb_apps)]

    def _generate_tcpu_o_tio(self, apps: list) -> list:
        """Generate a list of values between 0 and 1 that correspond to ratios
        of time spend doing I/O for each application.  The list
        complies to target occupancy provided at class creation, so
        the created workload has an occupancy close to desired
        occupancy.

        The function creates an Uniform Law U(x) where the mean of
        1/(1+U(x)) is equal to target occupancy times number of I/O
        ressources, by finding an upper bound. Lower bound is 0.

        Then it creates a list of len(apps) values
        1/(1+U(x)).

        :param apps:
        :returns:

        """

        assert (len(apps) > 0)
        assert (len(apps) == self.nb_apps)

        tcpu_o_tio = []
        K = len(apps)

        # get the mean of distribution law we target
        target_mean = self.target_theta * self.nb_ion / self.nb_apps
        assert (target_mean < 0.99 and target_mean > 0)

        # get upper bound of uniform law, that respects target mean
        upper_bound = self.find_uniform_upper(target_mean)
        lower_bound = 0
        # get alpha values
        alphas = np.random.uniform(low=lower_bound, high=upper_bound, size=K)

        # get tio(1) for each application
        tcpu_o_tio = [a for a in alphas]

        #sanity checks, we toss this set if it does not make sense
        for rio in tcpu_o_tio:
            if rio < 0.001:
                return None

        # for data
        # get mean io load generated
        tio = [1/ (1+t) for t in tcpu_o_tio]
        print("Load generated is in theory: ", sum(tio))
        # save generation data
        # write header if not existing
        if not os.path.exists(TIO_DATA):
            with open(TIO_DATA, 'w') as fd:
                csv_header = "n_app,n_node,target_ioload,upper_bound,target_mean, r_io\n"
                fd.write(csv_header)
                
        with open(TIO_DATA, 'a') as fd:
            csv_data = f"{self.nb_apps},{self.nb_ion},{self.target_theta},{upper_bound},{target_mean},{mean(tio)}\n"
            fd.write(csv_data)

        return tcpu_o_tio

    def uniform_upper_formula(self, b: float) -> float:
        return 1 / b * log(1 + b)

    def find_uniform_upper(self, mean: float):
        """Dichotomique find of the x that solves equation 1/x * log(x+1) =
        mean. It is used to find upper bound of uniform law U(y) that
        allows equation 1/(1+y) to have a mean around mean (or N.Occ
        in real case).

        :param mean:
        :returns:

        """
        approx_mean = np.float32(-1)
        approx_upper_bound = np.float32(10_000)
        approx_lower_bound = np.float32(0)
        mean = np.float32(mean)
        tolerance = np.float32(0.000001)

        while not (approx_mean < mean + tolerance * mean
                   and mean - mean * tolerance < approx_mean):
            approx_bound = (approx_upper_bound + approx_lower_bound) / 2
            assert approx_bound < approx_upper_bound

            approx_mean = self.uniform_upper_formula(approx_bound)
            if approx_mean < mean:
                approx_upper_bound = approx_bound
            else:
                approx_lower_bound = approx_bound

        # save data into csv
        # write header if not existing
        if not os.path.exists(OUTPUT_BOUND_DATA):
            with open(OUTPUT_BOUND_DATA, 'w') as fd:
                csv_header = "n_app,n_node,target_ioload,upper_bound,mean,approx_mean\n"
                fd.write(csv_header)
                
        with open(OUTPUT_BOUND_DATA, 'a') as fd:
            csv_data = f"{self.nb_apps},{self.nb_ion},{self.target_theta},{approx_bound},{mean},{approx_mean}\n"
            fd.write(csv_data)
        
        return approx_bound

    def get_apps(self):
        return self.gen_applications()
