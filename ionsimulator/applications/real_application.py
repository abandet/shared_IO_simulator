"""
Class that represents applicaiton with a profils bdw / n
"""
from typing import Final
from copy import deepcopy
import numpy as np
from ionsimulator.applications.application_interface import ApplicationInterface


class RealApplication(ApplicationInterface):
    """ Real application class"""
    _ID = 0

    def __init__(self,
                 nb_iter: int,
                 ni: int,
                 ni_arr: list,
                 bdw: list,
                 nb_phase: int,
                 tcpu_o_tio: float,
                 ncpu: int,
                 knowledge_bdw: list = None):
        # check if desired ni is within application profil
        if ni not in ni_arr:
            raise ValueError("ni not found in ni array for RealApplication")
        self.is_knowledge = knowledge_bdw is not None
        self.ni = ni
        self.ni_arr = deepcopy(ni_arr)
        self.bdw_arr = deepcopy(bdw)
        self.nb_phase = nb_phase
        self.tcpu_o_tio: Final[float] = tcpu_o_tio
        self.init_nb_iter = nb_iter
        self.init_ri: Final[float] = 1/(1+tcpu_o_tio)
        self.init_ni: Final[int] = ni
        self.tcpu: Final[int] = int(nb_iter - (nb_iter / (1+self.tcpu_o_tio)))

        self.v: Final[float] = (nb_iter / (1+self.tcpu_o_tio)) * \
            self.bdw_arr[self.ni_arr.index(self.ni)]
        if self.is_knowledge:
            self.knowledge_bdw = list(knowledge_bdw)
            self.v_knowledge: Final[float] = (
                nb_iter / (1+self.tcpu_o_tio)) * self.knowledge_bdw[self.ni_arr.index(self.ni)]

        self.stress_arr = self._init_stress_arr()
        self.tcpu_o_tio = tcpu_o_tio

        super().__init__(nb_iter, self.tcpu_o_tio, ni, ncpu=ncpu)

    def set_id(self, app_id: int):
        """Set application id. Must be unique.

        :param id: unique id
        :returns: 

        """
        self.id = app_id

    def get_bdw_k(self) -> float:
        """Exactly like get_bdw but this function returns knowledge bandwidth
        is is_knowledge was True at init

        :returns:

        """
        if not self.is_knowledge:
            return self.get_bdw()

        return self.knowledge_bdw[self.ni_arr.index(self.ni)]

    def get_bdw(self) -> float:
        """Return actuel bandwidth, i.e. with actual ni, of this application

        :returns: 

        """
        try:
            ni_idx = self.ni_arr.index(self.ni)
        except ValueError as e:
            print(
                f"Cannot find ni value in ni array for real application ({e})")
            raise e

        return self.bdw_arr[ni_idx]

    def get_ni_array(self) -> list:
        return self.ni_arr

    # def get_bdw_array(self) -> list:
    #     return self.bdw_arr

    def get_bdw_array_k(self) -> list:
        """Returns bandwidth array. Knowledge safe.

        :returns: 

        """
        if self.is_knowledge:
            return self.knowledge_bdw
        return self.bdw_arr

    def get_rio(self) -> float:
        """Compute and return the ratio of time application does I/O operation
        before run.
        :returns:


        """
        tcpu = self.tcpu
        tio = self.v / self.get_bdw()
        return tio / (tcpu+tio)

    def get_rio_k(self) -> float:
        """Compute and return the ratio of time application does I/O operation
        before run. Knowledge safe.
        :returns:


        """
        tcpu = self.tcpu
        if self.is_knowledge:
            tio = self.v_knowledge / \
                self.knowledge_bdw[self.ni_arr.index(self.ni)]
        else:
            tio = self.v / self.bdw_arr[self.ni_arr.index(self.ni)]

        return tio / (tcpu+tio)

    def get_stress(self) -> float:
        """ Compute the I/O stress, n*tiocf(n) / (tiocf(n)+tcpu)

        :returns: 

        """
        tio = self.v / self.get_bdw()
        tcpu = self.tcpu
        n = self.ni

        return n*tio/(tio+tcpu)

    def get_io_stress_at_nsys(self) -> float:
        """Like get_stress but with application nsys

        :returns: 

        """
        nsys = self.get_nsys()
        tio = self.v / self.bdw_arr[self.ni_arr.index(nsys)]
        tcpu = self.tcpu

        return nsys*tio/(tio+tcpu)

    def reconfigure_ni(self, ni: int) -> None:
        """Change application ni. It recompute IO time and change iteration
        list

        :param ni: new number of I/O resource.
        :returns: None

        """
        if ni == self.ni:
            return

        self.ni = ni
        tio = int(self.v / self.get_bdw())  # to do after ni update!
        self.nb_iter = tio + self.tcpu
        self.iters = self._init_iter_array()

    def _init_stress_arr(self):
        """ Return io load for each application configuration
        in form of list that follows self.ni_arr order

        :returns:

        """
        stress_arr = []
        for n, bdw in zip(self.ni_arr, self.bdw_arr):
            tio = self.v / bdw
            stress = (n*tio) / (tio+self.tcpu)
            stress_arr.append(stress)

        return stress_arr

    def _init_iter_array(self):
        nb_io = int(self.v / self.bdw_arr[self.ni_arr.index(self.ni)])

        # create io phase vector
        io_vec = [1] * nb_io
        assert io_vec.count(1) == nb_io
        io_vec = list(np.array_split(io_vec, self.nb_phase))

        cpu_vec = [0] * self.tcpu
        assert cpu_vec.count(0) == self.tcpu
        cpu_vec = list(np.array_split(cpu_vec, self.nb_phase))

        # assemble iter vector
        iteration_vec = []
        for i in range(len(io_vec)):
            for c in cpu_vec[i]:
                iteration_vec.append(c)
            for io in io_vec[i]:
                iteration_vec.append(io)

        try:
            assert iteration_vec.count(1) == nb_io
            # nb_iter +- 1
            assert len(iteration_vec) in [
                self.nb_iter - 1, self.nb_iter, self.nb_iter + 1]
        except AssertionError as e:
            print(
                f"Incorrect number of IO iteration in iterarray init: io({iteration_vec.count(1)}, iter({len(iteration_vec)}))")
            raise e

        return iteration_vec

    def __str__(self):
        return f"RealApplication<{self.id}>(ni={self.ni}, tcpu_o_tio={self.tcpu_o_tio}, nb_phase={self.nb_phase}, bdw_arr={self.bdw_arr}, tcpu={self.tcpu}, V={self.v})"

    def get_mean_bandwidth(self, stop_iteration):
        """
        Return the mean bandwith of the application during
        its runtime, until stop_iteration

        :param stop_iter: Iteration for the end of the study
        :return: I/O stretch metric
        """
        bdw = self.get_bdw()
        tio = self.run_iters[:stop_iteration].count(
            1) / stop_iteration  # Get io fraction of time
        return bdw * tio

    # def get_io_vol(self, stop_iteration: int) -> float:
    #     """
    #     Returns the amount of I/O transfered until stop iteration.
    #     This compute the number of I/O,
    #     """
    #     bdw = self.get_bdw()
    #     io_total_it = self.run_iters[:stop_iteration].count(1)
    #     return bdw * io_total_it

    # def single_cpu_load(self, ni) -> float:
    #     if self.is_knowledge:
    #         tio = self.v_knowledge / self.knowledge_bdw[self.ni_arr.index(ni)]
    #     else:
    #         tio = self.v / self.bdw_arr[self.ni_arr.index(ni)]

    #     return self.tcpu * self.ncpu / (tio + self.tcpu)

    # def single_io_stress(self, ni) -> float:
    #     """TODO describe function

    #     :param ni:
    #     :returns:

    #     """
    #     tcpu = self.tcpu
    #     if self.is_knowledge:
    #         tio = self.v_knowledge / self.knowledge_bdw[self.ni_arr.index(ni)]
    #     else:
    #         tio = self.v / self.bdw_arr[self.ni_arr.index(ni)]

    #     return tio * ni / (tio+tcpu)

    def get_nperf(self):
        """Return the number of forwarder that maximize application bandwidth.
        That configuration is called nperf in paper.

        :returns: Number of ION with max bandwidth

        """
        max_bdw = max(self.bdw_arr)
        max_bdw_idx = self.bdw_arr.index(max_bdw)

        return self.ni_arr[max_bdw_idx]

    def get_nperf_k(self):
        """Returns Nper, knowledge safe.

        :returns: 

        """
        if self.is_knowledge:
            return self.ni_arr[self.knowledge_bdw.index(max(self.knowledge_bdw))]

        return self.ni_arr[self.bdw_arr.index(max(self.bdw_arr))]

    def get_nsys(self):
        """Return Nsys that minimise application I/O load

        :returns:

        """
        min_stress = min(self.stress_arr)
        min_stress_idx = self.stress_arr.index(min_stress)
        return self.ni_arr[min_stress_idx]

    def get_nsys_k(self):
        """ Returns nsys according to knowledge.

        :returns: 

        """
        if not self.is_knowledge:
            return self.get_nsys()

        stress_arr = []
        for n, bdw in zip(self.ni_arr, self.knowledge_bdw):
            tio = self.v_knowledge / bdw
            stress = (n*tio) / (tio+self.tcpu)
            stress_arr.append(stress)

        nsys_idx = stress_arr.index(min(stress_arr))
        return self.ni_arr[nsys_idx]

    def get_nodes(self):
        """

        :returns: All nodes application is scheduled on

        """
        nodes = []
        for i in self.node_iter:
            nodes.append(i["node_id"])

        return nodes

    def get_io_efficiency(self, stop_iteration: int):
        """Return the io stretch, meaning the volume of I/O transfered until
        stop iteration compare to the volume it would have been
        transfered in nperf congestion free scenario.

        :param stop_iteration:
        :returns:

        """
        stop_iteration = min(stop_iteration, len(self.run_iters))

        # case ni = nperf
        if self.ni == self.get_nperf():
            try:
                return self.run_iters[:stop_iteration].count(1) / self.iters[:stop_iteration].count(1)
            except ZeroDivisionError:
                return 1

        # get volume in congestion free
        # first get array of iteration
        nperf = self.get_nperf()
        app = deepcopy(self)
        app.reconfigure_ni(nperf)
        ideal_iteration_arr = app.iters
        # count iteration and extrapolation with bandwidth
        max_bdw = max(self.bdw_arr)
        ideal_volume = ideal_iteration_arr[:stop_iteration].count(1) * max_bdw
        # get volume transferred during simulation
        current_bdw = self.get_bdw()
        sim_io_volume = self.run_iters[:stop_iteration].count(1) * current_bdw
        # Case no volume tranfered in time window
        if ideal_volume == 0:
            try:
                assert sim_io_volume == 0
                return 1
            except AssertionError:
                print(f"I/O volume should be 0, but {sim_io_volume} founded")

        # Because reconfigure_ni could add some small error in volume
        # Check if error is less than 5%
        if sim_io_volume >= ideal_volume:
            try:
                assert ideal_volume * 0.15 + ideal_volume >= sim_io_volume
                return 1
            except AssertionError:
                print(f"Ideal Volume: {ideal_volume}")
                print(f"Sim io volume: {sim_io_volume}")
        return sim_io_volume / ideal_volume

    def save_app_gen(self):
        """ Save application state at generation

        :returns: dict of application

        """
        data = {}
        data["id"] = self.id
        data["nsys"] = self.get_nsys()
        data["nperf"] = self.get_nperf()
        data["bdw_arr"] = self.bdw_arr
        if self.is_knowledge:
            data["k_bdw_arr"] = self.knowledge_bdw
        data["fwd_arr"] = self.ni_arr
        data["n_cpu"] = self.ncpu
        data["nb_iter"] = len(self.iters)
        data["tcpu"] = self.iters.count(0)
        data["tio"] = self.iters.count(1)
        data["v_io"] = self.v
        data["nb_phase"] = self.nb_phase

        return data

    def save_app_run(self, stop_it: int):
        """ Save application after simulation

        :returns:

        """
        data = {}
        stop_it = min(stop_it, len(self.run_iters))
        data["id"] = self.id
        data["stop_it"] = stop_it
        data["ideal_io_t"] = self.iters[:stop_it].count(1)
        data["io_t"] = self.run_iters[:stop_it].count(1)
        data["cpu_t"] = self.run_iters[:stop_it].count(0)
        data["tcp_o_tio"] = self.tcpu_o_tio
        data["bdw_arr"] = self.bdw_arr
        data["nb_col"] = self.run_iters[:stop_it].count(-1)

        # get data for all nodes
        data["node_data"] = []
        for i in self.node_iter:
            d = {}
            d["node_id"] = i["node_id"]
            d["node_col"] = i["node_iters"][:stop_it].count(-1)
            # d["node_vector"] = i["node_iters"][:stop_it]

            data["node_data"].append(d)

        assert len(data["node_data"]) == self.ni
        assert data["io_t"] <= data["ideal_io_t"]

        return data
