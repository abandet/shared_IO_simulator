import os
import json
import sqlite3
import statistics

I_DATA_PATH = "../data/out/"
DB_PATH = "../data/data.db"

# bound for strech analysis
LOWER_BOUND = 20
UPPER_BOUND = 1000


class Session:
    """ Represent one session, aka one json file
    log it into sqlite3 database"""

    def __init__(self, filename, filepath):
        # load json data
        with open(filepath, 'r') as fd:
            json_str = fd.read()
            self.data = json.loads(json_str)

        # get experiment settings
        filename = filename.split('_')
        self.allocator = filename[0]
        self.distributor = filename[1]
        self.nb_ion = int(filename[2])
        self.session_n = int(filename[3].split(".")[0])
        self.nb_app = self.data["nb_apps"]

        # init experiment aggregate data
        self.mean_strch, self.gmean_strch, self.min_strch, self.max_strch, self.median_strch, self.std_strch = self.compute_strech()
        self.mean_th, self.gmean_th, self.min_th, self.max_th, self.median_th, self.std_th = self.compute_throughput()
        # (mean_use, min_use, max_use, median_use,
        #  std_use) = self.compute_utilization()

    def log_to_db(self, db_path):
        """ store aggregated data to database"""
        with sqlite3.connect(db_path) as con:
            cur = con.cursor()
            request = f"INSERT INTO sessions (allocator, distributor, nb_ion, session_n, nb_app, mean_strch, gmean_strch, min_strch, max_strch, median_strch, stdev_strch, mean_use, gmean_use, min_use, max_use, median_use, stdev_use, mean_th, gmean_th, min_th, max_th, median_th, stdev_th) VALUES ('{self.allocator}', '{self.distributor}', {self.nb_ion}, {self.session_n}, {self.nb_app}, {self.mean_strch}, {self.gmean_strch}, {self.min_strch}, {self.max_strch}, {self.median_strch}, {self.std_strch}, 0, 0, 0, 0, 0, 0, {self.mean_th}, {self.gmean_th}, {self.min_th}, {self.max_th}, {self.median_th}, {self.std_th})"
            cur.execute(request)
            con.commit()
        # raise NotImplementedError

    def _aggregate_data(self, data: list) -> tuple:
        """ aggregates data with mean, max..."""
        mean = statistics.mean(data)
        median = statistics.median(data)
        maxi = max(data)
        mini = min(data)
        stdev = statistics.stdev(data)
        geo_mean = statistics.geometric_mean(data)
        return (mean, geo_mean, mini, maxi, median, stdev)

    def compute_utilization_and_collision(self, data) -> (tuple, tuple):
        """ return a list of all (ion utilization, nb_colision)"""
        ions = data["ionodes"]
        ions_ut = [0] * len(ions)
        ions_col = [0] * len(ions)
        for ion in ions:
            ion_ut = [0] * (UPPER_BOUND - LOWER_BOUND)
            ion_col = [0] * (UPPER_BOUND - LOWER_BOUND)
            for app_id in ion["apps"]:

        raise NotImplementedError

    def compute_strech(self) -> tuple:
        """ return a list of all application strech"""
        strechs = []
        for app in self.data["applications"]:
            strechs.append(self._find_strech(app))

        return self._aggregate_data(strechs)

    def compute_throughput(self):
        """ return system throughput over simulation """
        analysis_len = UPPER_BOUND - LOWER_BOUND + 1
        throughput = [0] * analysis_len
        for app in self.data["applications"]:
            vec = app["iteration"][LOWER_BOUND:UPPER_BOUND+1]
            bdw = app["parameters"]["bdw"]
            for idx, e in enumerate(vec):
                throughput[idx] += bdw

        return self._aggregate_data(throughput)

    def _find_strech(self, app_data):
        """ Find strech based on application data finded in json
        format."""
        iter_vec = app_data["iteration"]
        # simulation may not run all iteration
        nb_idle = iter_vec[LOWER_BOUND:UPPER_BOUND+1].count(-1)
        analysis_len = UPPER_BOUND - LOWER_BOUND
        nb_running = analysis_len - nb_idle

        return analysis_len / nb_running


if __name__ == "__main__":
    # create database
    # empty the file
    with open(DB_PATH, "w") as fd:
        pass
    # create table
    with open('create_table.sql', 'r') as sql_file, sqlite3.connect(DB_PATH) as con:
        sql_script = sql_file.read()
        cur = con.cursor()
        cur.execute(sql_script)
        con.commit()

    for filename in os.listdir(I_DATA_PATH):
        sess = Session(filename, I_DATA_PATH+filename)
        sess.log_to_db(DB_PATH)
