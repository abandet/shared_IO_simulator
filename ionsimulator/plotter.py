import json
import pandas as pd
from glob import glob
import numpy as np
import argparse
import os
from statistics import mean
import matplotlib.pyplot as plt
import seaborn as sns
import pdb
import math
from matplotlib.ticker import ScalarFormatter # for log scale

SELECTED_ALLOCATORS=["StaticAllocator1", "TCPUAllocator", "RandomAllocator", "NiSystem", "BestBdw", "MCKPAllocator"]

allocators = ["Static", "TCPU-Allocator", "Random", "Nsys-Allocator", "BestBdw", "MCKP"]
colors = sns.color_palette(n_colors=len(allocators))

palette = {allocator: color for allocator, color in zip(allocators, colors)}

sns.set_style("darkgrid", {"grid.color": ".6", "grid.linestyle": ":"})


def to_categoriel_load(data: pd.Series) -> pd.Series:
    """Transforme continuous load space into categorie of +- 0.05.  It
    round the value to the closest decimal (1). 0.249 become 0.2 and
    0.251 become 0.3.

    :param data: 
    :returns:

    """
    data = data.round(1)
    return data
    

def lambda_name_change(x: str) -> str:
    """Lambda function that return right name. If x not found is
    permutation it function returns x

    :param x: allocator or placement name from simulation
    :returns: allocator or placement name with paper convention

    """
    # Allocators
    if x == "StaticAllocator1":
        return "Static"
    if x == "RandomAllocator":
        return "Random"
    if x == "TCPUAllocator":
        return "TCPU-Allocator"
    if x == "NiSystem":
        return "Nsys-Allocator"
    if x == "MCKPAllocator":
        return "MCKP"

    # Placement
    if x == "RandomDistributor":
        return "Random-Placement"

    return x

def change_names(data: pd.DataFrame) -> pd.DataFrame:
    """Change data allocators name according to paper name.

    :param data: 
    :returns: 

    """
    data["allocator"] = data["allocator"].apply(lambda_name_change)
    data["placement"] = data["placement"].apply(lambda_name_change)
    return data


def compute_load(n_node: int, theta: float) -> float:
    """Convert a Theta and number of load from 

    :param n_app: 
    :param n_node: 
    :param theta: 
    :returns: 

    """
    return theta*n_node

def compute_load(load: float, n_node: int) -> float:
    return load / n_node

def filter_allocators(data: pd.DataFrame) -> pd.DataFrame:
    """Remove data where allocator is not in selected allocators

    :param data: 
    :returns: 

    """
    data = data[data["allocator"].isin(SELECTED_ALLOCATORS)]
    return data

def plot_saturation_states(data: pd.DataFrame, savepath: str) -> None:
    """print the tab with % of xp with occ > 1 and mean occ per allocator

    :param data: 
    :returns: 

    """
    data = data[data["is_saturated_gen"] != 1]
    data["target_occ"] = data["target_ioload"] / data["n_node"]
    # create columns for occ > 1
    data["is_saturated_xp"] = data["scheduler_occ"] > 1 + 0.01
    data = data.sort_values(by="allocator")
    # get data
    grouped_proportion = (data.groupby(by=["allocator", "target_occ"], as_index=False)["is_saturated_xp"].mean())

    # plot data
    fig, axs = plt.subplots(1, 2, figsize=(10, 5), sharex=True, layout='constrained')
    sns.lineplot(data=grouped_proportion, x="target_occ", y="is_saturated_xp", hue="allocator", ax=axs[0], palette=palette)
    # adding h line at 1 on mean occ (showing saturation line)
    axs[1].axhline(y=1, xmin=0, xmax=1, linestyle='--', color='red')
    sns.lineplot(data=data, x="target_occ", y="scheduler_occ", hue="allocator", ax=axs[1], errorbar=("pi", 80), err_style="band", palette=palette)
    
    # text for saturated undersaturation
    axs[1].text(x=0.08, y=1.05, s="Saturation", color='red')
    axs[1].text(x=0.08, y=0.90, s="No Saturation", color='red')
    # set axis name
    axs[0].set_xlabel(r"$\Theta$ at $\pi_{sys}$")
    axs[0].set_ylabel("Proportion of saturated allocation")
    axs[1].set_xlabel(r"$\Theta$ at $\pi_{sys}$ ($10^{th}$ to $90^{th}$ percentiles)")
    axs[1].set_ylabel(r"Mean allocations' $\Theta$")
    # remove legend from left plot
    axs[0].get_legend().remove()
    # save figure
    plt.tight_layout()
    path = os.path.join(savepath, "saturated_combined.png")
    plt.savefig(path)


def get_app_slowdown(sim_data: dict, gen_data: dict, stop_iter) -> (float, float):
    T = sim_data["io_t"] + sim_data["nb_col"]
    b_perf = max(gen_data["bdw_arr"])
    ni = len(sim_data["node_data"])
    b_n = gen_data["bdw_arr"][gen_data["fwd_arr"].index(ni)]
    V = sim_data["io_t"] * b_n
    assert (b_n <= b_perf)

    # case no collision and ni = nperf
    if b_n == b_perf and sim_data["nb_col"] == 0:
        return 1, 0
    
    try:
        rho_j = T / (V/b_perf)
    except ZeroDivisionError:
        raise Exception(f"Found B_perf = 0 or no I/O time (T) ({T})")
    rho_io = b_perf / b_n
    rho_col = rho_j - rho_io
    #  case no collision
    if sim_data["nb_col"] == 0:
        rho_col = 0

    assert(rho_j >= 1)
    assert(rho_col >= -0.000000001) # avoid precision problems
    return (rho_io, rho_col)
    

def get_mean_slowdown(gen_data:dict, data: dict) -> float:
    pj_io = []
    pj_col = []
    stop_it = data["stop_iter"]

    for g, s in zip(gen_data["apps_gen"], data["app_simulation"]):
        io, col = get_app_slowdown(s, g, stop_it)
        pj_io.append(io)
        pj_col.append(col)

    return mean(pj_io), mean(pj_col)

def get_max_slowdown(gen_data:dict, data: dict) -> float:
    pj_io = []
    pj_col = []
    stop_it = data["stop_iter"]

    for g, s in zip(gen_data["apps_gen"], data["app_simulation"]):
        io, col = get_app_slowdown(s, g, stop_it)
        pj_io.append(io)
        pj_col.append(col)

    return max(pj_io), max(pj_col)

def get_max_io_stretch(data: dict) -> float:
    max_stretch = 1
    for app in data["app_simulation"]:
        max_stretch = max(app["stretch"], max_stretch)

    return max_stretch

def get_mean_io_stretch(data: dict) -> float:
    stretch = [a["stretch"] for a in data["app_simulation"]]
    return mean(stretch)

def get_bandwidth_throttle_metric(gen_data:dict, data:dict) -> float:
    max_bdw = []
    actual_bdw = []
    for a in data["app_simulation"]:
        app_id = a["id"]
        app_ni = len(a["node_data"])
        for a_gen in gen_data["apps_gen"]:
            if a_gen["id"] == app_id:
                max_bdw.append(max(a_gen["bdw_arr"]))
                bdw_idx = 0
                if app_ni == 2:
                    bdw_idx == 1
                elif app_ni == 4:
                    bdw_idx == 2
                elif app_ni == 8:
                    bdw_idx == 4
                        
                actual_bdw.append(a_gen["bdw_arr"][bdw_idx])

    max_bdw = np.array(max_bdw)
    actual_bdw = np.array(actual_bdw)

    arr = actual_bdw / max_bdw
    return np.average(arr)

def get_io_load_spread(data: dict) -> float:
    min_load = 999999999
    max_load = -1
    for node in data["node_simulation"]:
        uti = node["t_io"] / (node["t_io"] + node["t_cpu"])
        min_load = min(min_load, uti)
        max_load = max(max_load, uti)

    return max_load - min_load

def get_machine_idle_time(gen_data: dict, run_data: dict) -> float:
    idle_t = 0
    Qcpu = 0
    simulation_t = run_data["stop_iter"]
    # Get app IO time * NB cpu node
    for g_app, r_app in zip(gen_data["apps_gen"], run_data["app_simulation"]):
        io_t = simulation_t - r_app["cpu_t"]
        ncpu = g_app["n_cpu"]
        Qcpu += ncpu
        idle_t += io_t * ncpu

    
    return idle_t / (Qcpu*simulation_t)

def get_colision_metrics(data: dict) -> float:
    col = [n["nb_col"] for n in data["node_simulation"]]
    return mean(col), sum(col)

def get_rio_spread(js: dict) -> float:
    """Sum rio on every nodes then compute the difference between nodes
    with max sum(rio) and min sum(rio)


    :param js: 
    :returns:

    """
    sum_rios = [n["sum_rio"] for n in js["node_simulation"]]
    return max(sum_rios) - min(sum_rios)
    
def load_file(json_file: str, json_folder: str) -> pd.DataFrame:
    data = {}
    xp_id = json_file.split("/")[-1].split("_")[0]
    gen_json = os.path.join(json_folder, "generation", f"{xp_id}.json")
    with open(json_file, 'r') as fd:
        js = json.load(fd)
        data["I/O_spread"] = get_io_load_spread(js)
        data["allocator"] = js["allocator"]
        data["placement"] = js["placement"]
        data["scheduler"] = js["allocator"] + " " + js["placement"]
        rio_arr = [a["ideal_io_t"]/(a["ideal_io_t"]+a["cpu_t"]) for a in js["app_simulation"]]
        data["mean_rio"] = mean(rio_arr)
        data["max_rio"] = max(rio_arr)
        data["min_rio"] = min(rio_arr)
        data["rio_spread"] = get_rio_spread(js)

        with open(gen_json, 'r') as gen_fd:
            gen_data = json.load(gen_fd)
            data["machine_idle_time"] = get_machine_idle_time(gen_data, js)
            data["bdw_throttle"] = get_bandwidth_throttle_metric(gen_data, js)
            data["mean_io_slowdown_io"], data["mean_io_slowdown_col"] = get_mean_slowdown(gen_data, js)
            data["max_io_slowdown_io"], data["max_io_slowdown_col"] = get_max_slowdown(gen_data, js)
            data["mean_io_slowdown"] = data["mean_io_slowdown_io"] + data["mean_io_slowdown_col"]
            data["max_io_slowdown"] = data["max_io_slowdown_io"] + data["max_io_slowdown_col"]
            data["target_ioload"] = gen_data["target_load"]
            data["sim_ioload"] = gen_data["IO_load"] / gen_data["n_node"]
            data["n_node"] = gen_data["n_node"]
            data["target_theta"] = gen_data["target_load"] / gen_data["n_node"]
            data["scheduler_occ"]= js["scheduler_load"] / gen_data["n_node"]

        data["n_app"] = len(js["app_simulation"])
        data["scheduler_load"] = js["scheduler_load"]
        data["is_saturated_gen"] = data["sim_ioload"] / data["n_app"] > data["n_node"]
        data["mean_col"], data["total_col"] = get_colision_metrics(js)
        data["per_col"] = data["mean_col"] / js["stop_iter"] * 100
    df = pd.DataFrame(data, index=[0])
    return df

def load_folder(folderpath: str) -> pd.DataFrame:
    files = glob(os.path.join(folderpath, "*"))
    data = pd.DataFrame()
    for f in files:
        try:
            if not "generation" in f:
                d = load_file(f, folderpath)
                # get occ
                xp_id = os.path.split(f)[-1].split("_")[0]
                gen_json = os.path.join(folderpath, "generation", f"{xp_id}.json")
                with open(gen_json, 'r') as fd:
                    js = json.load(fd)
                    d["IO_load"] = js["IO_load"]
                    d["n_node"] = js["n_node"]
                    d["f_gen"] = gen_json
                    d["f_sim"] = f
                    data = pd.concat([data, d])
        except Exception as e:
            print(f"Cannot load file {f}. Skipping it.")
            print(e)
            raise Exception()
    return data

def plot(data: pd.DataFrame, savepath: str, occ: list) -> None:
    # keep only 10, 20 and 40 number of application
    sns.set(font_scale=1.5)
    
    # sort scheduler by alphabetical order
    sched = sorted(set(data["scheduler"]))
    labels = sorted(set(data["allocator"]))
    for m in ["machine_idle_time"]:
        for p in set(data["placement"]):
            d = data[(data["placement"] == p) | (data["placement"] == 'MCKP')]
            metric_name = "Machine-Idletime"
            for i, o in enumerate(occ):
                fig, ax = plt.subplots(figsize=(8, 8))
                # Cut data according to occupancy tolerance policy
                plot_d = filter_data_by_occ(d, o)
                plot_d = plot_d.sort_values(by="allocator")
                if plot_d.empty:
                    break
                sns.boxplot(plot_d, x="n_app", y=m, hue="allocator", ax=ax, palette=palette)
                if m in ["mean_io_slowdown", "max_io_slowdown"]:
                    ax.set_yscale('log', base=2)

                ax.set_ylim((0.2, 1))
                ax.set_xlabel("Number of applications")
                ax.set_ylabel(metric_name)

                # keep legend for last only
                if i != 1:
                    ax.get_legend().remove()
                else:
                    ax.legend_.set_title(None)

                plt.tight_layout()
                fig_path = os.path.join(savepath, f"{m.replace('/', '')}_{p}_{o}.png")
                fig.savefig(fig_path)
                plt.close(fig)

def get_load_tolerances(n_app: int, n_node: int, theta: float) -> (float, float):
    if math.isnan(n_app):
        return 0, 0
    lower_occ, upper_occ = get_occ_tolerances(theta)
    lower_load, upper_load = compute_load(n_node, lower_occ), compute_load(n_node, upper_occ)

    return lower_load, upper_load

def filter_data_by_occ(data: pd.DataFrame, target_occ: float) -> pd.DataFrame:
    # Cut data according to occupancy tolerance policy
    # plot_d = data[data.apply(lambda x: x['sim_ioload'] < get_load_tolerances(x['n_app'], x['n_node'], target_occ)[1], axis=1)]
    # plot_d = plot_d[plot_d.apply(lambda x: x['sim_ioload'] > get_load_tolerances(x['n_app'], x['n_node'], target_occ)[0], axis=1)]

    tolerance = 0.05
    lower_bound = target_occ - 0.05
    upper_bound = target_occ + 0.05

    data = data[data["sim_ioload"] <= upper_bound]
    data = data[data["sim_ioload"] >= lower_bound]

    return data
            
def plot_placement(data: pd.DataFrame, savepath: str, occ: list) -> None:
    """This function do one subplot on each metric with different
    placement side by side

    """
    # keep only 10, 20 and 40 number of application
    auth_n_app = [10, 20, 40]
    data = data[data["n_app"].isin(auth_n_app)]
    data = data[data["n_node"] == 20]
    data = data.sort_values("allocator")
    
    for m in ["I/O_spread", "machine_idle_time", "per_col", "total_col", "mean_io_slowdown", "max_io_slowdown", "scheduler_occ"]:
        for o in occ:
            # Cut data according to occupancy tolerance policy
            plot_d = filter_data_by_occ(data, o)

            fig, axs = plt.subplots(nrows=1, ncols=len(set(data["placement"])), sharey=True, figsize=(20, 6))
            for i, p in enumerate(set(data["placement"])):
                d = plot_d[plot_d["placement"] == p]
                if d.empty:
                    break
                d = d.sort_values(by="placement")
                metric_name = m.replace("_", " ")
                sns.boxplot(d, x="n_app", y=m, hue="allocator", ax=axs[i], palette=palette)
                axs[i].set_title(f"Placement = {p}")
                axs[i].get_legend().remove()

            handles, labels = axs[0].get_legend_handles_labels()
            fig.legend(handles, labels, loc='upper right')
            fig_path = os.path.join(savepath, f"PLACEMENT_{m.replace('/', '')}_{o}.png")
            fig.tight_layout()
            fig.savefig(fig_path)
            plt.close(fig)


def plot_alloc_placement(data: pd.DataFrame, savepath: str, occ: float) -> None:
    auth_n_app = [10, 15, 20, 40]
    data = data[data["n_app"].isin(auth_n_app)]
    data = data[data["n_node"] == 20]

    alloc = ["Random", "Nsys-Allocator", "BestBdw"]
    sns.set(font_scale=2.3)
    occ = [0.5]
    for o in occ:
        plot_d = filter_data_by_occ(data, o)
        for m in ["I/O_spread"]:
            for i, a in enumerate(alloc):
                fig, ax = plt.subplots(nrows=1, ncols=1, sharey=True, figsize=(7,8))
                d = plot_d[plot_d["allocator"] == a]

                d = d.sort_values(by="placement")
                sns.boxplot(d, x="n_app", y=m, hue="placement", ax=ax)
                ax.set_xlabel("Number of applications")
                ax.set_ylabel("I/O-Spread")
                ax.set_ylim((0, 1))
                if i != 1:
                    ax.get_legend().remove()
                else:
                    ax.legend_.set_title(None)

                fig_path = os.path.join(savepath, f"diff_placement_{m.replace('/', '')}_{o}_{a}.png")
                fig.tight_layout()
                fig.savefig(fig_path)
                plt.close(fig)


def plot_alloc_placement_per_occ(data: pd.DataFrame, savepath: str, occ: float) -> None:
    alloc = ["Random", "Nsys-Allocator", "BestBdw", "Static"]
    alloc = ["BestBdw"]

    sns.set(font_scale=2.3)
    # for m in ["mean_io_slowdown", "machine_idle_time", "I/O_spread"]:
    for m in ["I/O_spread"]:
        for a in alloc:
            for i, o in enumerate(occ):
                fig, ax = plt.subplots(nrows=1, ncols=1, sharey=True, figsize=(7,8))
                plot_d = filter_data_by_occ(data, o)
                d = plot_d[plot_d["allocator"] == a]
                d = d.sort_values(by="placement")

                sns.lineplot(d, x="n_app", y=m, hue="placement", ax=ax, marker="o")
                ax.set_xlabel("Number of applications")
                ax.set_ylim((0.1,0.9))
                if i != 1:
                    ax.get_legend().remove()
                else:
                    ax.legend_.set_title(None)
                    ax.legend(loc=(0.05, 0.5))

                
                ax.set_ylabel("I/O-spread")
                fig_path = os.path.join(savepath, f"diff_placement_occ_{m.replace('/', '')}_{a}_{o}.png")
                plt.tight_layout()
                fig.savefig(fig_path)
                plt.close(fig)


def plot_alloc_occ_line(data: pd.DataFrame, savepath: str) -> None:
    """ Plot regression line of real occupancy on CPU load and mean I/O slowdown

    :param data: 
    :param savepath: 
    :returns: 

    """
    sns.set(font_scale=1.1)
    data = data[data["placement"] != "MCKPAllocator"]
    data = data.sort_values(by="allocator")
    scheduler = ["BestBdw GreedyClairvoyant", "BestBdw GreedyNonClairvoyant", "TCPUAllocator GreedyNonClairvoyant"]
    data = data[data["scheduler"].isin(scheduler)].sort_values(by="scheduler")
    data = data[data["cat_load"] <= 1]


    for m in ["mean_io_slowdown", "machine_idle_time"]:
        if m == "mean_io_slowdown":
            m_name = "Mean-I/O-SlowDown"
        else:
            m_name = "Machine-Idletime"
        # plot reparated figures
        fig, ax = plt.subplots(figsize=(5,5))
        reg_data = data
        g = sns.lineplot(data=reg_data, x="cat_load", y=m, hue="scheduler", errorbar=('pi', 80), ax=ax)
        ax.set_xlabel(r"I/O-Load($\pi_{sys}$)")
        ax.set_ylabel(m_name)
        ax.legend(title='')
        if m == "mean_io_slowdown":
            g.get_legend().remove()
        # ax.set_ylim((0,17.5))

        fig_path = os.path.join(savepath, f"occ_alloc_{m.replace('/','')}.png")
        fig.tight_layout()
        fig.savefig(fig_path)



def plot_barchart_slowdown(data: pd.DataFrame, savepath: str, tested_occ: list) -> None:
    """Plot bar chart, with separation for p_j col et p_j io.
    One color per allocator algorithm and x axis represents number of application

    :param data: 
    :param savepath: 
    :returns: 

    """
    sns.set(font_scale=1.5)
    # data = data[data["placement"].isin(["MCKPAllocator", "GreedyNonClairvoyant"])]
    data = data[data["placement"].isin(["GreedyNonClairvoyant", "MCKP"])]
    data.loc[data["allocator"] == "MCKP", "allocator"] = " MCKP" # to move MCKP at first
    data = data.sort_values(by="allocator").reset_index(drop=True)
    
    fig, axs = plt.subplots(1,2, sharex=True, sharey=True, figsize=(10, 6))
    
    for col, theta in enumerate([tested_occ[0], tested_occ[-1]]):
        d = filter_data_by_occ(data, theta)
        d = d.groupby(by=["allocator"], as_index=False)[["mean_io_slowdown_io", "mean_io_slowdown_col"]].mean()
        
        # for row, metrics in enumerate([("mean_io_slowdown_io", "mean_io_slowdown_col"), ("max_io_slowdown_io", "mean_io_slowdown_col")]):
        metrics = ["mean_io_slowdown_io", "mean_io_slowdown_col"]
        ax = axs[col]
        rho = {"io": d[metrics[0]], "col": d[metrics[1]]}
        bottom = np.zeros(len(set(d["allocator"])))
        for label, values in rho.items():
            if label == "col":
                label_name = r"$\rho_{con}$"
            else:
                label_name = r"$\rho_{io}$"
            ax.bar(d["allocator"], list(values), 0.5, label=label_name, bottom=bottom)
            bottom += values
        if col == 0:
            ax.legend(loc="upper right")
        if col == 0:
            ax.set_ylabel(metrics[0].split("_")[0] + r" $\rho_j$")
        if col == 0:
            ax.set_xlabel(r'$0.15 \leq$ ' + r"I/O-Load($\pi_{sys}$)" + r" $< 0.25$")
        if col == 1:
            ax.set_xlabel(r'$0.75 \leq$ ' + r"I/O-Load($\pi_{sys}$)" + r" $< 0.85$")
        ax.set_ylim((1,70))
        ax.tick_params('x', labelrotation=45)
        ax.set_yscale('log', base=2)
        ax.yaxis.set_major_formatter(ScalarFormatter())
            
    plt.tight_layout()
    save_at = os.path.join(savepath, "user_impact_barchart.png")
    plt.savefig(save_at)


def plot_slowdown_lines(data: pd.DataFrame, savepath: str) -> None:

    sns.set(font_scale=1.5)
    data["real_theta"] = data["IO_load"] / data["n_node"]
    data = data.sort_values(by="allocator")
    reg_order = 3

    # plot left figure (rho io)
    fig, ax = plt.subplots(1,1, figsize=(5,8))
    for alloc in set(data["allocator"]):
        color = palette[alloc]
        d = data[data["allocator"] == alloc]
        sns.regplot(d, x="real_theta", y="mean_io_slowdown_io", ax=ax, order=reg_order, scatter=False, label=alloc, color=color)
        ax.set_ylabel(r"Mean $\rho_{io}$")
        ax.set_xlabel(r"I/O-Load($\pi_{sys}$)")
        ax.set_ylim((0,5))
        
    plt.tight_layout()
    save_at = os.path.join(savepath, "user_impact_effi_io_lines.png")
    plt.savefig(save_at)
            
    # # plot right figure (rho colision)
    # plot left figure (rho con)
    fig, ax = plt.subplots(1,1, figsize=(6,10))
    for alloc in set(data["allocator"]):
        color = palette[alloc]
        d = data[data["allocator"] == alloc]
        sns.regplot(d, x="real_theta", y="mean_io_slowdown_col", ax=ax, order=reg_order, scatter=False, label=alloc, color=color)
        ax.set_ylabel(r"Mean $\rho_{col}$")
        ax.set_xlabel(r"I/O-Load($\pi_{sys}$)")
        ax.set_ylim((0,5))
        ax.legend(loc="upper left")
        
    plt.tight_layout()
    save_at = os.path.join(savepath, "user_impact_effi_con_lines.png")
    plt.savefig(save_at)
        

def plot_ioload_real_vs_generated(data: pd.DataFrame, savepath: str):
    """ Generate box of generated scenario function to number of application

    :param data: 
    :returns: 

    """
    data["diff_ioload"] = data["target_ioload"] - data["IO_load"]
    sns.boxplot(data=data, x="target_ioload", y="diff_ioload", hue="n_app")
    
    save_fig = os.path.join(savepath, "diff_io_load.png")
    plt.suptitle("target - real")
    plt.savefig(save_fig)

                

def get_occ_tolerances(occ: float) -> (float, float):
    occ_tolerance = 0.02
    upper_occ = occ + occ_tolerance
    lower_occ = occ - occ_tolerance

    return (lower_occ, upper_occ)



DEFAULT_DATA_PATH = None

if __name__=="__main__":
    parser = argparse.ArgumentParser(
        prog="Plotter Version4",
        description="")

    parser.add_argument('-i',
                        "--input",
                        help="Input data folder",
                        required=True)

    parser.add_argument('-o',
                        "--output",
                        help="Output data csv",
                        required=True)


    parser.add_argument('--load', help="(Re) load files from input data to generate csv", action="store_true")

    args = parser.parse_args()

    DEFAULT_DATA_PATH = args.output
    CREATE_DATA = args.load

    folders = glob(f"{args.input}/*")
    df = pd.DataFrame()
    if CREATE_DATA or not os.path.exists(DEFAULT_DATA_PATH):
        print("Create data.")
        for i,f in enumerate(folders):
            if not "img" in f:
                print(f" Loading folder {i+1}/{len(folders)}", end='\r')
                data = load_folder(f)
                if data is not None:
                    df = pd.concat([df, data])
        print()
        df = change_names(df)
        df["cat_load"] = to_categoriel_load(df["sim_ioload"])
        df.to_csv(DEFAULT_DATA_PATH, sep=",")

    else:
        print("Load data.")
        df = pd.read_csv(DEFAULT_DATA_PATH, sep=',')
        print("Data loaded.")


    if not os.path.exists(args.input):
        raise ValueError(f"Folder {args.input} does not exist")

    savepath = os.path.join(args.input, "img")
    if not os.path.exists(savepath):
        os.mkdir(savepath)

    tested_occ = [0.2, 0.5, 0.8]

    # print mean tio spread per placement
    # grouped = df.groupby(by=["placement", "target_theta"], as_index=False)["rio_spread"].max()
    # print(grouped)    

    # plot_slowdown_lines(df, savepath)
    # plot_barchart_slowdown(df, savepath, tested_occ)
    # plot_saturation_states(df, savepath)
    # plot(df, savepath, tested_occ)
    # plot_placement(df, savepath, tested_occ)
    plot_alloc_occ_line(df, savepath)
    # plot_alloc_placement(df, savepath, tested_occ)
    # plot_alloc_placement_per_occ(df, savepath, tested_occ)
    # plot_ioload_real_vs_generated(df, savepath)

