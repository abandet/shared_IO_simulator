"""
Greedy clairvoyant placement policy
"""
import operator
from applications.application_interface import ApplicationInterface
from io_node import IONode

class GreedyClairvoyant:
    """ Clairvoaynt placement class"""
    def __init__(self):
        self.name = "GreedyClairvoyant"
        self.nb_ion = -1

    def _get_app_score(self, app: ApplicationInterface) -> float:
        """Returns score of application on one I/O node, which is the current
        rio of the applicaiton. It is knowledge safe (take knowledge app into account).

        :param app:

        """
        return app.get_rio_k()

    def _get_node_score(self, node: IONode) -> float:
        """ Returns the current score of node, meaning the sum of rio on it.

        :param node:

        """
        apps = node.get_apps()
        score = 0
        for app in apps:
            score += self._get_app_score(app)

        return score

    def _find_emptiest_node(self, nodes: list, app: ApplicationInterface) -> IONode:
        """Returns the emptiest node according to sum policy. It compute
        score on all node then return the node with the lowest score.

        :param nodes: 
        :param app:

        """
        # find first node for comparaison
        emptiest_node = None
        emptiest_score = 0
        for node in nodes:
            if not node.is_app_in(app):
                emptiest_node = node
                emptiest_score = self._get_node_score(emptiest_node)
                break

        if emptiest_node is None:
            raise ValueError(
                "Default node not founded in _find_emptiest_node (GreedyClairvoyant)")

        # main loop
        for node in nodes:
            score = self._get_node_score(node)
            if score < emptiest_score and not node.is_app_in(app):
                emptiest_score = score
                emptiest_node = node

        return emptiest_node

    def allocate_nodes(self, nb_ion: int, apps: list) -> list:
        """Generates I/O nodes and schedule application on it, according to
        clairvoyant policy.

        :param nb_ion: 
        :param apps:

        """
        apps = sorted(apps, key=operator.methodcaller('get_rio'), reverse=True)
        nodes = [IONode() for _ in range(nb_ion)]
        for app in apps:
            for _ in range(app.get_ni()):
                emptiest_node = self._find_emptiest_node(nodes, app)
                emptiest_node.add_app(app)

        assert len(nodes) >= 1
        return nodes

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()
