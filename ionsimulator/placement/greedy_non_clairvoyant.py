"""
Greedy Non Clairvoyant policy
"""
import operator
from ionsimulator.applications.application_interface import ApplicationInterface
from io_node import IONode


class GreedyNonClairvoyant:
    """ Greedy non clairvoaynt class"""

    def __init__(self):
        self.name = "GreedyNonClairvoyant"
        self.nb_ion = -1

    def _get_node_score(self, node: IONode):
        """Return number of application scheduled on node.

        :param node: 
        :returns: 

        """
        return len(node.get_apps())

    def _find_emptiest_node(self, nodes: list, app: ApplicationInterface) -> IONode:
        """ Returns the node with fewer application.

        :param nodes: 
        :param app: 

        """
        # find first node for comparaison
        emptiest_node = None
        emptiest_score = 0
        for node in nodes:
            if not node.is_app_in(app):
                emptiest_node = node
                emptiest_score = self._get_node_score(emptiest_node)
                break

        if emptiest_node is None:
            raise ValueError(
                "Default node not founded in _find_emptiest_node (GreedyNonClairvoyant)")

        # main loop
        for node in nodes:
            score = self._get_node_score(node)
            if score < emptiest_score and not node.is_app_in(app):
                emptiest_score = score
                emptiest_node = node

        return emptiest_node

    def allocate_nodes(self, nb_ion: int, apps: list) -> list:
        """ Generates I/O nodes with applications scheduled on it.

        :param nb_ion: 
        :param apps: 

        """
        apps = sorted(apps, key=operator.methodcaller('get_rio'), reverse=True)
        nodes = [IONode() for _ in range(nb_ion)]
        for app in apps:
            for _ in range(app.get_ni()):
                emptiest_node = self._find_emptiest_node(nodes, app)
                emptiest_node.add_app(app)

        return nodes

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()
