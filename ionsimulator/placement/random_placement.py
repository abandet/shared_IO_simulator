"""
Random placement policy
"""

from random import shuffle
from ionsimulator.applications.application_interface import ApplicationInterface
from io_node import IONode


class RandomPlacement:
    """ Random placement class"""

    def __init__(self):
        self.name = "RandomDistributor"
        self.nb_ion = -1

    def _find_emptiest_node(self, nodes: list, app: ApplicationInterface) -> IONode:
        """ Returns a random node where app in not scheduled on.

        :param nodes: 
        :param app: 

        """
        shuffle(nodes)
        for node in nodes:
            if app not in node.get_apps():
                return node

        raise ValueError("Cannot find emptiest node with random distributor.")

    def allocate_nodes(self, nb_ion: int, apps: list) -> list:
        """ Generates I/O nodes with applications scheduled on it"""
        nodes = [IONode() for _ in range(nb_ion)]
        for app in apps:
            for _ in range(app.get_ni()):
                emptiest_node = self._find_emptiest_node(nodes, app)
                emptiest_node.add_app(app)

        if not nodes:
            raise ValueError(
                "Nodes list is empty for RandomDistributor. Cannot allocate nodes")

        assert len(nodes) >= 1
        return nodes

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()
