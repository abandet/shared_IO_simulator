"""
Script that create experiments setup (applications sets) and save in it in generation folder
"""
import argparse
import os
import json
import numpy as np
import pandas as pd
from random import seed
from applications.appset_generator import RealAppsSetGenerator, RealApplication

# type hint
App_list = list[RealApplication]


def io_load_pisys(apps: App_list) -> float:
    """Returns I/O load of application set at pi_sys

    :param apps: 
    :returns: 

    """
    load = 0
    for a in apps:
        load += a.get_io_stress_at_nsys()

    return load

def save_generation(savepath: str, apps: int, nb_node: int, target_occ: float):
    data = {}
    data["apps_gen"] = []
    for a in apps:
        # a.reconfigure_ni(a.get_nsys())
        data["apps_gen"].append(a.save_app_gen())
    # get real occ
    ioload = 0
#    data["real_occ"] = compute_occ(apps, nb_node)
    data["IO_load"] = io_load_pisys(apps)
    data["target_load"] = target_occ * nb_node
    data["n_node"] = nb_node

    with open(savepath, 'w') as fd:
        json.dump(data, fd)

def generate_app_set(occ: float, n_node: int, n_app: int, data: pd.DataFrame, n_iter: int, knowledge: bool) -> App_list:
    """ Generate an application set that comply to function settings

    :param occ: Target Occupancy
    :param n_node: Number of I/O resources
    :param n_app: Number of applications
    :param data: Application set data (marenostrum_forge or Ost)
    :param n_iter: Number of iteration for each application
    :returns: list of application object, length of list is n_app

    """
    app_gen = RealAppsSetGenerator(occ, n_node, n_app, data, n_iter, is_knowledge=knowledge)
    apps = app_gen.gen_applications()
    if apps is not None:
        assert(len(apps) == n_app)

    return apps

def is_scenario_possible(n_app: int, n_node: int, occ: float) -> bool:
    """Return true if the scenario with n_app applications, n_node I/O
    resources at occ occupancy is possible with tio < 1.

    This means n_app / n_node < occ

    :param n_app: Number of applicaiton in scenario
    :param n_node: Number of I/O resources in scenario
    :param occ: Target OCC for scenario
    :returns: True if scenario is possible, false otherwise

    """
    return n_app / n_node > occ


if __name__=="__main__":
    parser = argparse.ArgumentParser(
    prog="IO Node CP generator",
    description="Generate and save applications set")

    parser.add_argument("-c",
                        "--config",
                        help="configuration file",
                        type=str,
                        required=True)


    args = parser.parse_args()

    # load parameters of generation
    with open(args.config, 'r') as f:
        config = json.load(f)

    if "is_knowledge" in config:
        is_knowledge = config["is_knowledge"] == 1
    else:
        is_knowledge = False

    if not "random_seed" in config:
        print("Error! Give me a pseudo-random number generation seed!")
        exit()
    seed(config["random_seed"])

    # node configuration
    if "node_list" in config:
        node_range = config["node_list"]
    else:
        node_range = range(config["min_nb_node"], config["max_nb_node"], config["node_step"])
    # app_configuration
    if "app_list" in config:
        app_range = config["app_list"]
    else:
        app_range = range(config["min_nb_app"], config["max_nb_app"], config["app_step"])
    # occ_configuration
    if "occ_list" in config:
        occ_range = config["occ_list"]
    else:
        occ_range = np.arange(config["min_occ"], config["max_occ"], config["occ_step"])
    # load data
    data = pd.read_csv(config["input_data"])
    data = data[data["forwarders"] != 0]  # remove forwarders = 0 lines

    nb_iteration = 5000
    if "nb_iteration" in config:
        nb_iteration = config["nb_iteration"]


    gen_data = pd.DataFrame()
    # target theta loop
    for occ in occ_range:
        # number of node loop
        for nb_node in node_range:
            # number of app loop
            for nb_app in app_range:
                # assert generation is possible. Skip it otherwise
                if occ*nb_node/nb_app > 1:
                    print(f"I dont want to generate sets with {occ} {nb_app} {nb_node} because that makes the average rio too large {occ*nb_node/nb_app}")
                    continue
                xp_id = 0
                # generates untile having right amount of workload
                while xp_id < config["nb_xp"]:
                    if not is_scenario_possible(nb_app, nb_node, occ):
                        print(f"Scenario with {nb_app} application, {nb_node} resources and {occ} occupancy is not possible (rio would be greater than 1.). Skipping this generation.")
                        break

                    # find file and folder name for new data json
                    foldername = f"{occ:.2f}_{nb_app}_{nb_node}"
                    folderpath = os.path.join(config["output_folder"], foldername, "generation")
                    savepath = os.path.join(folderpath, f"{xp_id}.json")
                    os.makedirs(folderpath, exist_ok=True)
                    new_gen_data = pd.DataFrame([dict(occ=occ, n_node=nb_node, n_app=nb_app, xp_id=xp_id)])
                    gen_data = pd.concat([new_gen_data, gen_data], ignore_index=True)

                    if not os.path.exists(savepath):
                        print(f"Generating application with occ={occ:.2f}, #app={nb_app} and #node={nb_node}")
                        apps = generate_app_set(occ, nb_node, nb_app, data, nb_iteration, knowledge=is_knowledge)
                        if apps is None:
                            print(f"Failed to create scenario {occ} {nb_app} {nb_node}")
                            continue
                        else:
                            xp_id+=1
                            save_generation(savepath, apps, nb_node, occ)


                            print(f"Saved at {savepath}.")
                    else:
                        print(f"{savepath} already exists. Skip.")
                        xp_id+=1

    if not gen_data.empty:
        gen_data = gen_data.astype(dict(n_node=int, n_app=int, xp_id=int))
        gen_data = gen_data.reindex(np.random.permutation(gen_data.index)) # shuffle
        datapath = os.path.join(config["output_folder"], "xp_gen_data.csv")
        gen_data.to_csv(datapath, index=False)
    else:
        raise Exception("No data has been write")
