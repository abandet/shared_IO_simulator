""" 
This script launch test with fixes number of application.
Occupancy is determined with lower number of I/O nodes (#ION) setted
This script is multi threaded, with one thread per #ION.
"""
import sys
from concurrent.futures import ProcessPoolExecutor
from applications.appset_generator import RealAppsSetGenerator
from applications.real_application import RealApplication
from allocator.best_ni_allocator import BestBdwAllocator, BestNiSmartAllocator, NiSystem, BestBdwPerNode
from allocator.random_allocator import RandomAllocator
from allocator.static_allocator import StaticAllocator
from allocator.tcpu_allocator import TCPUAllocator
from placement.greedy_clairvoyant import GreedyClairvoyant
from placement.greedy_non_clairvoyant import GreedyNonClairvoyant
from placement.random_placement import RandomPlacement
from MCKP import MCKPScheduler
from Machine import Machine
import pandas as pd
from copy import deepcopy
import json
import pdb
import csv
import argparse
import os
import shutil


O_DATA_PATH = "./data/out/"  # Only for JSON output
I_DATA_PATH = "data/marenostrum_forge.csv"  # Input data for bdw per forwarder

def compute_load(apps: list):
    stress = [a.get_stress() for a in apps]
    load = sum(stress)

    return load

def get_simulation_data(apps, ions, stop_it) -> dict:
    d = {}
    d["app_simulation"] = []
    d["stop_iter"] = stop_it
    # Application data
    for a in apps:
        app_data = a.save_app_run(stop_it)
        d["app_simulation"].append(app_data)

    # Resources data
    d["node_simulation"] = []
    for n in ions:
        node_data = n.save_node_run(stop_it)
        d["node_simulation"].append(node_data)

    d["scheduler_load"] = compute_load(apps)
    return d

def load_apps(jsonpath: str) -> list:
    with open(jsonpath, 'r') as f:
        data = json.load(f)

    apps = []
    for a in data["apps_gen"] :
        tcpu_o_tio = a["tcpu"] / a["tio"]
        if "k_bdw_arr" in a:
            knowledge_bdw = a["k_bdw_arr"]
        else:
            knowledge_bdw = None
        app = RealApplication(a["nb_iter"], 1, a["fwd_arr"],
                              a["bdw_arr"], a["nb_phase"],
                              tcpu_o_tio, a["n_cpu"], knowledge_bdw)
        app.set_id(a["id"])
        apps.append(app)

    return apps

def run(savepath, allocators, distributors, others, nb_node: int,
        base_occ: float, nb_app: int, stop_iteration: int,
        nb_xp: int, start_idx: int=0) -> None:
    """ runs experiments with a determined number of nodes"""
    # find occupancy
    occ = base_occ
    header = True
    # Load output folder
    filename = f"{occ:.2f}_{nb_app}_{nb_node}"
    local_folder = os.path.join(output_folder, f"{filename}")

    # load generation folder
    local_json_folder = os.path.join(local_folder, "generation")
    if not os.path.exists(local_json_folder):
        raise Exception(f"No generation folder founded: {local_json_folder}")

    sim_data = dict()
    for n in range(start_idx, start_idx + nb_xp):
        # generates application set
        json_gen = os.path.join(local_json_folder, f"{n}.json")
        # allocator loop
        for allocator in allocators:
            # distribute nodes
            for distributor in distributors:
                # Check if simulation was already executed
                local_json_sim = os.path.join(local_folder, f"{n}_{allocator}_{distributor}.json")
                if os.path.exists(local_json_sim):
                    print(f"{local_json_sim} already exists. Skipping simulation.")
                    continue

                # loading application from generation json
                apps = load_apps(json_gen)
                assert(len(apps)==nb_app)
                
                app_conf = None  # Needed for TCPUAllocator
                if str(allocator) == "TCPUAllocator":
                    app_conf = allocator.find_ni(apps, nb_node)

                for idx, app in enumerate(apps):
                    # find best ni according to allocator policy
                    if str(allocator) == "TCPUAllocator":
                        best_ni = app_conf[idx]
                    else:
                        best_ni = allocator.find_ni(app,
                                                    nb_ion=nb_node,
                                                    nb_apps=len(apps))
                    app.reconfigure_ni(best_ni)

                nodes = distributor.allocate_nodes(nb_node, apps)

                # create machine
                machine = Machine(0, [None],
                                  str(distributor),
                                  stop_iteration=3000)

                machine.set_apps(apps)
                machine.set_ionodes(nodes)

                #sim_occ = compute_occ(apps, len(nodes), stop_iteration)
                # run experiments
                machine.run(stop_iteration)
                # save experiments
                local_json_sim = os.path.join(local_folder, f"{n}_{allocator}_{distributor}.json")
                sim_data = get_simulation_data(apps, nodes, 4000)
                sim_data["allocator"] = str(allocator)
                sim_data["placement"] = str(distributor)
                #sim_data["simulation_occ"] = sim_occ
                with open(local_json_sim, 'w') as fd:
                    json.dump(sim_data, fd)


        for other in others:
            local_json_sim = os.path.join(local_folder, f"{n}_{other}.json")
            if os.path.exists(local_json_sim):
                print(f"{local_json_sim} already exists. Skipping simulation.")
                break

            # loading application from generation json
            apps = load_apps(json_gen)
            assert(len(apps)==nb_app)

            nodes = other.apply(nb_node, apps)
            machine = Machine(0, [None], None, stop_iteration=3000)
            #assert check_distribution(nodes, apps)
            machine.set_ionodes(nodes)
            machine.set_apps(apps)
            machine.run(stop_iteration)
            sim_data = get_simulation_data(apps, nodes, 4000)
            sim_data["allocator"] = str(other)
            sim_data["placement"] = str(other)
            with open(local_json_sim, 'w') as fd:
                json.dump(sim_data, fd)


def check_distribution(nodes, apps):
    """
    Check if all apps in param apps are all scheduled on nodes.
    TODO: check ni are respected

    :param nodes: I/O nodes with scheduled app
    :param apps: Applications set to schedule
    :return: True if all application are present in I/O nodes, False otherwise
    """
    # transform into set
    app_on_node = [a.id for n in nodes for a in n.apps]
    set_app_node = set(app_on_node)
    set_app = set([a.id for a in apps])

    return set_app_node == set_app


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="IO Node Runner",
        description="Run experiments with one number of I/O node")

    # base xp settings
    parser.add_argument("-n",
                        "--nb_node",
                        type=int,
                        help="Number of I/O node",
                        required=True)

    parser.add_argument("-o",
                        "--output",
                        help="Output directory (csv files)",
                        default="test")

    parser.add_argument("--occ",
                        type=float,
                        help="Machine Occupancy",
                        required=True)

    parser.add_argument("-a",
                        "--nb_app",
                        help="Number of application",
                        type=int,
                        default=20)

    parser.add_argument("-x",
                        "--nb_xp",
                        help="Number of experiment to run on each scheduler",
                        type=int,
                        default=50)

    parser.add_argument("-s",
                        "--stop_iteration",
                        help="Stop iteration for metric measure",
                        type=int,
                        default=5000)

    parser.add_argument("--start_id",
                        help="Start iteration id. Mean for splitting experiments.",
                        type=int,
                        default=0)

    parser.add_argument("--mckp", action='store_true', help="Adding MCKP experiments")

    # define a dictionnary with all possible allocator option
    # key is the option name and value is the scheduler
    allocator_choice = {"bestbdw": BestBdwAllocator(), "bestbdwsmart": BestNiSmartAllocator(0.4), "randomallocator": RandomAllocator(), "nsys": NiSystem(), "tcpu": TCPUAllocator(), "bestbdwnode": BestBdwPerNode(), "static": StaticAllocator()}
    # adding all allocator as option
    for k, v in allocator_choice.items():
        parser.add_argument(f"--{k}", help=f"Adding allocator {v} to experiments.", action='store_true')


    args = parser.parse_args()
    
    output_folder = args.output
    
    DISTRIBUTOR = [GreedyNonClairvoyant(), GreedyClairvoyant(), RandomPlacement()]

    other_sched = []
    if args.mckp:
        other_sched.append(MCKPScheduler())

    allocator = []
    for alloc_name, alloc_class in allocator_choice.items():
        if alloc_name in vars(args):
            if getattr(args, alloc_name):
                allocator.append(alloc_class)
    
    run(output_folder, allocator, DISTRIBUTOR, other_sched, args.nb_node,
            args.occ, args.nb_app, args.stop_iteration,
            args.nb_xp, args.start_id)
