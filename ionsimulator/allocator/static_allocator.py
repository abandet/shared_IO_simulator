"""
Module with static allocator class
"""
from ionsimulator.allocator.allocator_interface import AllocatorInterface
from ionsimulator.applications.real_application import RealApplication
from ionsimulator.applications.appset_generator import NCPU


class StaticAllocator(AllocatorInterface):
    """ Allocates xN / K nodes per application, where x is the parameter
    nb_ion factor, N the number of ion and K the number of application on the machine"""

    def __init__(self, nb_ion_factor=1):
        self.factor = nb_ion_factor

    def find_ni(self, app: RealApplication, **kwargs) -> int:
        # kwargs check
        if "nb_ion" not in kwargs:
            raise ValueError(
                "Error in static allocation: number of resources not specified.")
        if "nb_apps" not in kwargs:
            raise ValueError(
                "Error in static allocation: number of applications not specified.")

        nb_ion = kwargs["nb_ion"]
        nb_apps = kwargs["nb_apps"]

        # /!\convention
        ncpu = nb_ion * NCPU
        ncpu_app = app.ncpu
        # return closest matching ni founded in application
        ni = min(app.ni_arr, key=lambda x: abs(
            x - (ncpu_app / ncpu * nb_apps)))
        return ni

    def __str__(self):
        return f"StaticAllocator"

    def __repr__(self):
        return self.__str__()
