"""
Random allocations policy.
"""

from random import choice
from copy import copy
from ionsimulator.allocator.allocator_interface import AllocatorInterface
from ionsimulator.applications.real_application import RealApplication


class RandomAllocator(AllocatorInterface):
    """ Allocates a random ni for each application"""

    def __init__(self):
        pass

    def find_ni(self, app: RealApplication, **kwargs) -> int:
        """Make a random choice of n of each application. It choses between
        all available configuration.

        :param app: Applicaiton to allocate
        :returns: Return a copy of n (avoid side effect if later modified)

        """

        return copy(choice(app.get_ni_array()))

    def __str__(self):
        return "RandomAllocator"

    def __repr__(self):
        return self.__str__()
