"""
Collection of allocator that used bandwidth profils:
    - BestBdw -> Best bandwidth
    - BestNiSmartAllocator -> Best Bandwith with cost under a threshold
    - BestBdwPerNode -> Best b(n) / n ratio
    - NiSystem -> Find and return nsys
"""
from math import exp
import numpy as np
from ionsimulator.allocator.allocator_interface import AllocatorInterface
from ionsimulator.applications.real_application import RealApplication


class BestBdwAllocator(AllocatorInterface):
    """Allocates the best number of forwarder to a dedicated
    application according to bandwith array."""

    def __init__(self):
        pass

    def find_ni(self, app: RealApplication, **kwargs):
        max_bdw_ni = app.get_nperf_k()
        return max_bdw_ni

    def __str__(self):
        return "BestBdw"

    def __repr__(self):
        return self.__str__()


class BestNiSmartAllocator(AllocatorInterface):
    """ Allocate number of forwarding nodes.
    Takes a threshold who defines by how much a single forwarding
    nodes should increase the bandwith performance in order to get
    allocated, following cost function 1 - exp( - gain / xost).
    Avoid allocation with  extra forwarding cost for small performance
    improvement.

    Allocator does not go throught all forwards configuration and stop
    at first configuration who does not match threshold criteria, taking the
    higher ni meeting threshold condition."""

    def __init__(self, threshold: float):
        self.thresh = threshold

    def find_ni(self, app: RealApplication, **kwargs):
        init_bdw = app.get_bdw_array_k()[0]
        init_ni = app.get_ni_array()[0]
        for bdw, ni in zip(reversed(app.get_bdw_array_k()), reversed(app.get_ni_array())):
            gain = bdw / init_bdw * 100
            cost = ni / init_ni * 100
            if self._cost_function(cost, gain) > self.thresh:
                return ni

        return init_ni

    def _cost_function(self, cost, gain):
        return 1 - exp(-(gain/cost))

    def __str__(self):
        return f"SmartBestNiAllocator_{self.thresh}"

    def __repr__(self):
        return self.__str__()


class BestBdwPerNode(AllocatorInterface):
    """
    Allocates the best number of forwarder to a dedicated
    application according to bandwith array, according to system
    point of view.

    It implements n sys, with argmin n / b(n)
    """

    def __init__(self):
        pass

    def find_ni(self, app: RealApplication, **kwargs):
        bdw_arr = np.array(app.get_bdw_array_k())
        ni_arr = np.array(app.get_ni_array())
        nsys = ni_arr / bdw_arr

        min_nsys_ni = np.argmin(nsys)

        # security check
        assert nsys[min_nsys_ni] == min(nsys)

        return ni_arr[min_nsys_ni]

    def __str__(self):
        return "BestBdwPerNode"

    def __repr__(self):
        return self.__str__()


class NiSystem(AllocatorInterface):
    """
    Allocates Nys to all application. Is knowledge safe.
    """

    def __init__(self):
        pass

    def find_ni(self, app: RealApplication, **kwargs):
        nsys = app.get_nsys_k()
        assert nsys is not None
        return nsys

    def __str__(self):
        return "NiSystem"

    def __repr__(self):
        return self.__str__()
