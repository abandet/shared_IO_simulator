"""
Module doc string
"""


class AllocatorInterface:
    """ Distributors class interface. Used to unified Distributors implementation.
    """

    def __init__(self):
        pass

    def find_ni(self, app, **kwargs):
        """Find and returns 1D array of size len(apps) (so K) with number of node
        for each apps in parameters.

        :param apps: Applications that needs allocation :returns: 1D
        array with number of I/O resources for each applications in
        apps
        """
        raise NotImplementedError

    def __str__(self):
        return "AllocationInterface"
