"""
TCPU allocator policy. 
Tries to maximise CPU load, while keeping I/O load under or equal to 1.
"""

from copy import deepcopy
from ionsimulator.allocator.allocator_interface import AllocatorInterface


class TCPUAllocator(AllocatorInterface):
    """ TCPU allocation class"""

    def __init__(self):
        pass

    def find_ni(self, apps: list, **kwargs):
        # kwargs check
        if "nb_ion"not in kwargs:
            raise ValueError(
                "Error in tcpu allocation: number of resource not specified in find ni function.")
        nb_ion = kwargs["nb_ion"]

        # 1 configure all app with nsys
        # 2 While loop - creates pi
        app_conf = [deepcopy(a.get_ni()) for a in apps]  # pi in paper
        done = False
        while not done:
            # initial configuration
            app_conf_cpy = deepcopy(app_conf)
            load_diff = len(app_conf) * [-1]
            init_load = self.compute_io_load(apps, app_conf_cpy)
            # find new ni for each app
            for i in range(0, len(app_conf)):
                n = app_conf_cpy[i]
                app = apps[i]
                nperf = app.get_nperf_k()
                while n != nperf and load_diff[i] < 0:
                    n = app.get_next_n(n)
                    new_load = init_load - app.single_io_stress(
                        app.get_ni()) + app.single_io_stress(n)
                    # found new ni that fullfill condition load < N
                    if new_load < nb_ion:
                        app_conf_cpy[i] = n
                        load_diff[i] = app.single_cpu_load(
                            n) - app.single_cpu_load(app_conf[i])
            idx = load_diff.index(max(load_diff))  # find argmax load_diff
            if load_diff[idx] < 0:
                done = True
            else:
                app_conf[idx] = app_conf_cpy[idx]

        return app_conf

    def compute_io_load(self, apps: list, nis: list) -> float:
        """ Returns I/O load as describe in paper

        :param apps: list of RealApplication
        :param nis: list of app configuration (pi in paper)
        :returns: I/O load

        """
        singles_load = [a.single_io_stress(nis[i]) for i, a in enumerate(apps)]
        return sum(singles_load)

    def __str__(self):
        return "TCPUAllocator"

    def __repr__(self):
        return self.__str__()
