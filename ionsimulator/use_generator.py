import matplotlib.pyplot as plt

x = [(x*2.5*0.2) / 20 for x in range(60)]
plt.plot(x)
plt.ylabel("Theorical use")
plt.xlabel("#I/O Applications")
plt.show()
