from io_node import IONode
from copy import deepcopy, copy
from random import shuffle
from statistics import median, mean
from scipy.stats import pearsonr
import pdb


class Machine:
    """ Simulate the machine (like a controller)"""

    def __init__(self, nb_ionodes: int, apps: list, scheduler,
                 stop_iteration: int):
        """Initialize the machine for simulation, with applicaiton
        and io nodes

        :param nb_ionodes: Number of I/O nodes in forwarding layer
        :param apps: Application to schedule on machine
        :param scheduler: Name of the scheduler (Not used)
        :param stop_iteration: Last iteration to considere for simulation

        """
        self.nb_ionodes = nb_ionodes
        self.ionodes = [IONode() for i in range(self.nb_ionodes)]
        self.apps = apps
        self.runtime_apps = []
        self.current_iter = 0
        self.sched = scheduler
        self.stop_iter = stop_iteration

    def get_pending_apps(self):
        """ Returns all app not scheduled """
        return self.apps

    def set_apps(self, apps: list):
        self.apps = apps

    def get_nb_ionodes(self):
        """ returns number of ionode """
        return self.nb_ionodes

    def get_ionodes(self):
        """ Returns list of IONodes in the machine """
        return self.ionodes

    def set_ionodes(self, nodes):
        self.ionodes = nodes

    def get_nth_ionode(self, n: int):
        """ Returns the nth ionode of the machine"""
        if n > len(self.ionodes):
            raise Exception(f"IONode {n} do not exist (get_nth_ionodes)")
        return self.ionodes[n]

    def generate_stretch_stats(self):
        """ Run complete iteration over io node and app """
        ion_arr = self.ionodes
        # generate app vectors
        active_apps = True
        nb_iter = 0
        while (active_apps or nb_iter < self.minimum_iteration):
            shuffle(ion_arr)
            active_apps = False
            for ion in ion_arr:
                active_apps = ion.next_iter() >= 0 or active_apps
            nb_iter += 1

    def is_active_app(self):
        for app in self.apps:
            if app.is_active():
                return True
        return False

    def run(self, stop_iter):
        for ion in self.ionodes:
            for app in ion.apps:
                app.add_node_iter_array(ion.id)

        # Run iterations
        # while (self.current_iter < self.stop_iter and self.is_active_app()):
        while (self.is_active_app() and self.current_iter < stop_iter):
            shuffle(self.ionodes)
            for ion in self.ionodes:
                ion.run_iter()
            for app in self.apps:
                app.run_iter()

            self.current_iter += 1

    def to_dict(self, scheduler=""):
        apps = [app.to_dict() for app in self.apps]
        ions = [ion.to_dict() for ion in self.ionodes]
        nb_apps = len(self.apps)
        nb_ionode = len(self.ionodes)

        dic = {
            "scheduler": scheduler,
            "nb_ionode": nb_ionode,
            "nb_apps": nb_apps,
            "ionodes": ions,
            "applications": apps
        }
        return dic

    def to_csv_row(self, stop_iter: int):
        """ Return a csv with metrics computed

        :param stop_iter: Iteration where data analyse stops
        :return: list of header, data in form of dicionnary where key are listed in header
        """
        start_iter = 20
        assert start_iter < stop_iter

        csv_data = dict()
        # Basic machine info
        csv_headers = ["nb_nodes"]
        csv_data["nb_nodes"] = len(self.ionodes)

        # IO stretch part
        csv_headers += [
            "min_io_stretch", "max_io_stretch", "mean_io_stretch",
            "median_io_stretch"
        ]
        app_stretch = [a.get_io_stretch(stop_iter) for a in self.apps]
        assert len(app_stretch) != 0
        csv_data["min_io_stretch"] = min(app_stretch)
        csv_data["max_io_stretch"] = max(app_stretch)
        csv_data["mean_io_stretch"] = mean(app_stretch)
        csv_data["median_io_stretch"] = median(app_stretch)

        # Bandwidth part
        app_mean_bdw = [a.get_mean_bandwidth(stop_iter) for a in self.apps]
        csv_headers += [
            "min_io_bdw", "max_io_bdw", "mean_io_bdw", "median_io_bdw"
        ]
        csv_data["min_io_bdw"] = min(app_mean_bdw)
        csv_data["max_io_bdw"] = max(app_mean_bdw)
        csv_data["mean_io_bdw"] = mean(app_mean_bdw)
        csv_data["median_io_bdw"] = median(app_mean_bdw)

        # Utilization data
        utilization_arr = [n.get_utilization(stop_iter) for n in self.ionodes]
        csv_headers += [
            "min_utilization", "max_utilization", "mean_utilization",
            "median_utilization", "utilization_spread", "utilization_arr"
        ]
        csv_data["min_utilization"] = min(utilization_arr)
        csv_data["max_utilization"] = max(utilization_arr)
        csv_data["mean_utilization"] = mean(utilization_arr)
        csv_data["median_utilization"] = median(utilization_arr)
        csv_data["utilization_spread"] = max(utilization_arr) - min(
            utilization_arr)
        csv_data["utilization_arr"] = utilization_arr

        # # CPU load
        # cpu_loads = [a.single_cpu_load() for a in self.apps]
        # csv_headers += ["min_cpu_load", "max_cpu_load", "mean_cpu_load"]
        # csv_data["min_cpu_load"] = min(cpu_loads)
        # csv_data["max_cpu_load"] = max(cpu_loads)
        # csv_data["mean_cpu_load"] = mean(cpu_loads)

        # Collision data
        collision_arr = [a.get_collision(stop_iter) for a in self.apps]
        csv_headers += [
            "min_collision", "max_collision", "mean_collision",
            "median_collision", "collision_spread"
        ]
        csv_data["min_collision"] = min(collision_arr)
        csv_data["max_collision"] = max(collision_arr)
        csv_data["mean_collision"] = mean(collision_arr)
        csv_data["median_collision"] = median(collision_arr)

        # io iteration ratio data
        io_iter_count = [a.get_io_ratio(stop_iter) for a in self.apps]
        csv_headers += ["mean_io_ratio", "max_io_ratio", "median_io_ratio"]

        csv_data["mean_io_ratio"] = mean(io_iter_count)
        csv_data["max_io_ratio"] = max(io_iter_count)
        csv_data["median_io_ratio"] = median(io_iter_count)

        # Amount of I/O transfered
        io_amount = [a.get_io_vol(stop_iter) for a in self.apps]
        csv_headers += ["mean_io_vol", "max_io_vol"]
        csv_data["mean_io_vol"] = mean(io_amount)
        csv_data["max_io_vol"] = max(io_amount)

        # Tio but on nodes
        nodes_tio = [
            n.get_tios() if len(n.get_tios()) > 0 else [0]
            for n in self.ionodes
        ]
        mean_node_tio = [mean(n) for n in nodes_tio]
        # get inner tio spread
        inner_node_tio_spread = [max(n) - min(n) for n in nodes_tio]
        csv_headers += [
            "inner_node_tio_spread_mean", "inner_node_tio_spread_max"
        ]
        csv_data["inner_node_tio_spread_mean"] = mean(inner_node_tio_spread)
        csv_data["inner_node_tio_spread_max"] = max(inner_node_tio_spread)
        # get outter mean tio spread
        outter_node_tio_spread = max(mean_node_tio) - min(mean_node_tio)
        csv_headers += ["outter_node_tio_spread"]
        csv_data["outter_node_tio_spread"] = outter_node_tio_spread

        # Idle time
        csv_headers += ["machine_idle_time"]
        m_idle_time = [
            a.get_idle_time(start_iter, stop_iter) for a in self.apps
        ]
        csv_data["machine_idle_time"] = sum(m_idle_time)

        # # Machine data
        real_occ = [a.get_real_occ() for a in self.apps]
        csv_headers += ["real_occ"]
        csv_data["real_occ"] = sum(real_occ) / len(self.ionodes)

        return csv_headers, csv_data

    def save_generation_state():
        pass

    
