"""
compute the difference between two source of data (ost, knowledge, ion...) to generate tabs
"""
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
import pdb

INPUT_DATA_1 = "./data/plot_data4.csv"
INPUT_DATA_2 = "./data/plot_data4_k.csv"
OUTPUT_DATA = "./data/comparison_k.csv"


allocators = ["BestBdw GreedyClairvoyant", "TCPUAllocator GreedyNonClairvoyant", "BestBdw GreedyNonClairvoyant"]
colors = sns.color_palette(n_colors=len(allocators))
palette = {allocator: color for allocator, color in zip(allocators, colors)}
sns.set_style("darkgrid", {"grid.color": ".6", "grid.linestyle": ":"})


def categorise_gen_theta(data: pd.DataFrame) -> pd.DataFrame:
    """generates and round generation theta with one digit. This creates category of theta +-0.05

    :param data: 
    :returns: 

    """
    data["gen_theta"] = data["sim_ioload"]
    data["gen_theta"] = data["gen_theta"].round(1)

    return data


def compare_dataframes(main_df: pd.DataFrame, second_df: pd.DataFrame) -> pd.DataFrame:
    """Compute difference in % between main dataframe and an other
    dataframe. Its first compute the mean value of every metric then
    do (second - main) / main * 100 on every metrics.

    :param main_df: 
    :param second_df: 
    :returns:

    """
    metrics = ["mean_io_slowdown", "machine_idle_time"]
    # filter data to keep only good generation
    main_df = categorise_gen_theta(main_df)
    second_df = categorise_gen_theta(second_df)

    # compute means
    main_df_grouped = main_df.groupby(by=["scheduler", "gen_theta"])[metrics].mean()
    second_df_grouped = second_df.groupby(by=["scheduler", "gen_theta"])[metrics].mean()

    # # compute upper bound
    # # sort by sim ioload then compute diff
    # main_df_u = main_df.groupby(by=["scheduler", "sim_ioload", "gen_theta"], as_index=False)[metrics].mean()
    # second_df_u  = second_df.groupby(by=["scheduler", "sim_ioload", "gen_theta"], as_index=False)[metrics].mean()

    # print(second_df_u - main_df_u)

    ret = (second_df_grouped - main_df_grouped)
    return ret


def plot_comparison(data: pd.DataFrame) -> None:
    """Plot the comparison obtain between two dataframe, with rounded
    theta in x and metrics in yaxis

    :param data: 
    :returns:

    """
    metrics = ["mean_io_slowdown", "machine_idle_time"]

    sns.set(font_scale=1.1)
    for m in metrics:
        fig, ax = plt.subplots(1,1, figsize=(5,5))
        sns.lineplot(data, x="gen_theta", y=m, hue="scheduler", ax=ax)
        savepath = f"./data/comparison_{m}.png"
        ax.set_xlabel(r"I/O-Load($\pi_{sys}$)")
        ax.set_ylabel("Machine Idletime")
        ax.get_legend().remove()
        plt.tight_layout()
        plt.savefig(savepath)
        plt.close()

if __name__=="__main__":
    main_df = pd.read_csv(INPUT_DATA_1, sep=',')
    second_df = pd.read_csv(INPUT_DATA_2, sep=',')
    main_df = main_df[main_df["scheduler"].isin(allocators)]
    second_df = second_df[second_df["scheduler"].isin(allocators)]

    data = compare_dataframes(main_df, second_df)
    data.to_csv(OUTPUT_DATA)
    plot_comparison(data)
