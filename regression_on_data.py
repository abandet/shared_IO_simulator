"""
This little script test linear regression for applicaitons knowledge experiments
It create 4 categories of applications:
  - Ascent   : When the more I/O resources N, the more performance. Last N should have at least increases performances by D%
  - Descent  : When the more I/O resources N, the less performance. Last N should have at least decreases performances by D%
  - Peak     : When one numlber of I/O ressources N has at least D% more performance than first but is not the last node. But application is not already in other two caterories.
  - Neutral  : All others applications.

Percentage D is definied at beginning of the script

This script creates data and generates plot
"""
import pandas as pd
import numpy as np
import os
import seaborn as sns
import matplotlib.pyplot as plt


sns.set(font_scale=1.4)


FIG_PREFIX = "ion"
INPUT_DATA = "./data/marenostrum_forge.csv"
OUTPUT_DATA = "./data/augmented_marenostrum_forge.csv"

# FIG_PREFIX="ost"
# INPUT_DATA = "./data/ost_data.csv"
# OUTPUT_DATA = "./data/augmented_ost_data.csv"

# Macro for applicaiton categories
ASCENT = "ascent"
DESCENT = "descent"
PEAK = "peak"
NEUTRAL = "neutral"

# D
GAP_PERCENTAGE = 50 / 100

# Minimal number of different configuration to categories applications
MINIMAL_NB_CONF = 3

sns.set_style("darkgrid", {"grid.color": ".6", "grid.linestyle": ":"})


def get_bandwidth_list(app: pd.DataFrame) -> list:
    """Returns ordered list of bandwith of the application

    :param app: 
    :returns: 

    """
    app = app.sort_values("forwarders")
    return list(app["bandwidth"])


def is_ascent(bdw_profil: list) -> bool:
    """TODO describe function

    :param bdw_profil: 
    :returns: 

    """
    assert (len(bdw_profil) >= MINIMAL_NB_CONF)

    # first should be minimum
    if min(bdw_profil) != bdw_profil[0]:
        return False
    # last should be maximum
    if max(bdw_profil) != bdw_profil[-1]:
        return False
    # last bdw should be greater than first + D%
    if bdw_profil[-1] < bdw_profil[0] + bdw_profil[0] * GAP_PERCENTAGE:
        return False

    return True


def is_descent(bdw_profil: list) -> bool:
    """TODO describe function

    :param bdw_profil: 
    :returns: 

    """
    assert (len(bdw_profil) >= MINIMAL_NB_CONF)

    # first should be maximum
    if max(bdw_profil) != bdw_profil[0]:
        return False
    # last should be minimum
    if min(bdw_profil) != bdw_profil[-1]:
        return False
    # first bdw should be greater than last +D%
    if bdw_profil[0] < bdw_profil[-1] + bdw_profil[-1] * GAP_PERCENTAGE:
        return False

    return True


def is_peak(bdw_profil: list) -> bool:
    """TODO describe function

    :param bdw_profil: 
    :returns: 

    """
    assert (len(bdw_profil) >= MINIMAL_NB_CONF)

    max_bdw = max(bdw_profil)
    min_bdw = min(bdw_profil)

    # difference of at least D% between max and min
    if min_bdw + min_bdw * GAP_PERCENTAGE > max_bdw:
        return False
    # check max bandwidth is not the last or first value
    if max_bdw == bdw_profil[0] or max_bdw == bdw_profil[-1]:
        return False

    return True


def find_app_category(bdw_profil: list) -> str:
    """Returns the category of the application, in string format.

    :param bdw_profil: 
    :returns: String, definied by macros

    """

    if is_ascent(bdw_profil):
        return ASCENT
    elif is_descent(bdw_profil):
        return DESCENT
    elif is_peak(bdw_profil):
        return PEAK

    return NEUTRAL


def find_all_apps_categories(data: pd.DataFrame) -> pd.DataFrame:
    """TODO describe function

    :param data: 
    :returns: 

    """
    # adding extra column for categories
    data["profile"] = np.nan
    # extract all applications names
    scenarios = set(data["scenario"])
    for scenario in scenarios:
        app = data[data["scenario"] == scenario]
        bdw = get_bandwidth_list(app)
        category = find_app_category(bdw)
        data.loc[data["scenario"] == scenario, "profile"] = category

    return data


def annotate_cat(data: pd.DataFrame) -> pd.DataFrame:
    """Annotate category for plot on 2x2.
    coordinate (x, y):
       ASC -> (1,1)
       DEC -> (2,1)
       PEK -> (1,2)
       NEU -> (2,2)

    :param data: 
    :returns: 

    """
    data.loc[data["profile"] == ASCENT, "plot_col"] = 0
    data.loc[data["profile"] == DESCENT, "plot_col"] = 1
    data.loc[data["profile"] == NEUTRAL, "plot_col"] = 2
    data.loc[data["profile"] == PEAK, "plot_col"] = 3

    return data


def tab_founded_cat(data: pd.DataFrame) -> None:
    """Print how many of each category is founded in dataset

    :param data: 
    :returns: 

    """
    cat_counter = data.groupby(["profile"])["scenario"].count()
    print(cat_counter)


def plot_regression(data: pd.DataFrame, savepath: str) -> None:
    """TODO describe function

    :param data: 
    :param savepath: 
    :returns: 

    """
    sns.set(font_scale=2)
    facet_kws = dict(legend_out=False, sharex=True, sharey=True)
    plot = sns.lmplot(data=data, x="forwarders", y="norm_bdw",
                      hue="profile", col="plot_col", order=2, facet_kws=facet_kws)

    # plot settings
    plot.set_axis_labels("Number of forwarders", "Normalized bandwidth")

    plot.legend.remove()
    titles = ['Ascent', 'Descent', 'Peak', "Neutral"]
    for ax, title in zip(plot.axes.flatten(), titles):
        ax.set_title(title)

    # plot save
    fig_path = os.path.join(savepath, f"{FIG_PREFIX}_profils_regression.png")
    plot.savefig(fig_path)


def norm_apps_bdw(data: pd.DataFrame) -> pd.DataFrame:
    data["norm_bdw"] = data.groupby(
        ["scenario"])["bandwidth"].transform(lambda x: (x / x.max()))

    return data


def plot_profils(data: pd.DataFrame, savepath: str) -> None:
    """TODO describe function

    :param data: 
    :param savepath: 
    :returns: 

    """
    g = sns.FacetGrid(data=data, col="profile")
    g.map_dataframe(sns.lineplot, x="forwarders", y="norm_bdw", hue="scenario")
    # plot settings

    g.set(xticks=[1, 2, 4, 8])
    g.set_xticklabels(['1', '2', '4', '8'])
    g.set_axis_labels("", "Norm. bandw.")

    # plot save
    fig_path = os.path.join(savepath, f"{FIG_PREFIX}_profils.png")
    g.savefig(fig_path)


def get_knowlege_bdw(data: pd.DataFrame, profile: str) -> list:
    """Fit polynomial regression of degree 2 for a particular profil with
    available data in data. It returns a list of bandwidth value
    corresponfing to each forwarder value possible, sorted by number
    of forwarder.

    :param data: Applications data
    :param profil: Should be a string (peak, ascent...)
    with profil name in lower case 
    :returns: dict where key is number of forwarder and value the knowledge 
    bandwidth of this profile.
    """
    d = data[data["profile"] == profile]
    if d.empty:
        raise ValueError(
            f"No column profile or no profile named {profile} founded in get_knowlege_bdw")

    forwarders = set(data["forwarders"])
    bdw_dict = {}
    # fit data
    degree = 2
    x = d["forwarders"]
    y = d["norm_bdw"]
    reg = np.poly1d(np.polyfit(x, y, degree))
    # save poly value at n forwarder
    for n in sorted(forwarders):
        bdw_dict[n] = reg(n)

    return bdw_dict


if __name__ == "__main__":
    data = pd.read_csv(INPUT_DATA)
    data = data[data["forwarders"] != 0]  # Remove case with no forwarder

    data = find_all_apps_categories(data)
    data = norm_apps_bdw(data)

    # fit and note knwoledge bandwidth
    knowledge_bdw = {}
    for p in ["ascent", "descent", "peak", "neutral"]:
        knowledge_bdw[p] = get_knowlege_bdw(data, p)
    for i, row in data.iterrows():
        bdw = knowledge_bdw[row["profile"]][row["forwarders"]]
        data.at[i, 'knowledge_bdw'] = bdw

    data.to_csv(OUTPUT_DATA)

    data = annotate_cat(data)
    tab_founded_cat(data)
    plot_regression(data, "./data/")
    plot_profils(data, "./data/")
