CC = mpicc
CFLAGS = -Wall 
LDFLAGS =

TARGET = mpi_launcher mpi_launcher_ion mpi_launcher_ion_k mpi_launcher_ost

all: $(TARGET)

$(TARGET): %: %.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)


clean:
	rm -f $(TARGET)
